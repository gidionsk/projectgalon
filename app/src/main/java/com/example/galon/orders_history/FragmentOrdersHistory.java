package com.example.galon.orders_history;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.galon.R;
import com.example.galon.mySQL.RequestHandler;
import com.example.galon.classes.Htrans;
import com.example.galon.mySQL.Configuration;
import com.google.firebase.auth.FirebaseAuth;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

public class FragmentOrdersHistory extends Fragment {
    public FragmentOrdersHistory() {}

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_orders_history, container, false);
    }

    OrdersHistoryAdapter historyAdapter;
    RecyclerView rvHistory;
    Spinner spTahun,spStatus;
    TextView information;

    public ArrayList<Htrans> listHtrans = new ArrayList<>();
    String status_filter = "Semua";
    String tahun_filter = "";
    FirebaseAuth mAuth;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        rvHistory = view.findViewById(R.id.orderHistory_rvHistory);
        rvHistory.setHasFixedSize(true);
        rvHistory.setLayoutManager(new LinearLayoutManager(getContext()));

        spTahun = view.findViewById(R.id.orderHistory_spTahun);
        spStatus = view.findViewById(R.id.orderHistory_spStatus);
        information = view.findViewById(R.id.orderHistory_tvInformation);

        ArrayList<String> listtahun = new ArrayList<>();
        ArrayList<String> liststatus = new ArrayList<>();

        int tahun = Calendar.getInstance().get(Calendar.YEAR);
        for (int i = 2021; i <= 2025; i++) {
            listtahun.add(i+"");
        }
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getContext(),R.layout.spinner_item,listtahun);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spTahun.setAdapter(dataAdapter);

        for (int i = 0; i < listtahun.size(); i++) {
            if(Integer.parseInt(listtahun.get(i))==tahun){
                spTahun.setSelection(i);
            }
        }

        liststatus.add("Semua");
        liststatus.add("Menunggu");
        liststatus.add("Pengiriman");
        liststatus.add("Gagal");
        liststatus.add("Sukses");
        ArrayAdapter<String> dataAdapter2 = new ArrayAdapter<String>(getContext(),R.layout.spinner_item,liststatus);
        dataAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spStatus.setAdapter(dataAdapter2);

        getJSON(Configuration.URL_GET_HTRANS,status_filter,tahun_filter);

        mAuth = FirebaseAuth.getInstance();

        spStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                status_filter = spStatus.getSelectedItem().toString();
                tahun_filter = spTahun.getSelectedItem().toString();
                getJSON(Configuration.URL_GET_HTRANS,status_filter,tahun_filter);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spTahun.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                status_filter = spStatus.getSelectedItem().toString();
                tahun_filter = spTahun.getSelectedItem().toString();
                getJSON(Configuration.URL_GET_HTRANS,status_filter,tahun_filter);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    private String JSON_STRING;
    private void showHtrans() {
        JSONObject jsonObject = null;
        try {

            jsonObject = new JSONObject(JSON_STRING);
            JSONArray result = jsonObject.getJSONArray(Configuration.TAG_JSON_ARRAY);

            listHtrans.clear();

            for(int i = 0; i<result.length(); i++){
                JSONObject jo = result.getJSONObject(i);
                String tanggal = jo.getString("tanggal");
                String waktu = jo.getString("waktu");
                String quantity = jo.getString("quantity");
                String grand_total = jo.getString("grandtotal");
                String status_approval = jo.getString("status_approval");
                String status_pembayaran = jo.getString("status_pembayaran");
                String metode = jo.getString("metode");
                String id = jo.getString("id");
                String nama = jo.getString("nama_penerima");
                String alamat = jo.getString("alamat");
//                String nohp = jo.getString("");

                Htrans tempHtrans = new Htrans(id,tanggal,waktu,quantity,grand_total,status_approval,status_pembayaran,metode,nama,"0822555",alamat);
                listHtrans.add(tempHtrans);
                historyAdapter = new OrdersHistoryAdapter(getContext(),listHtrans);
                historyAdapter.notifyDataSetChanged();
            }
            rvHistory.setAdapter(historyAdapter);

            if(result.length()>0){
                rvHistory.setVisibility(View.VISIBLE);
                information.setVisibility(View.GONE);
            }
            else{
                rvHistory.setVisibility(View.GONE);
                information.setVisibility(View.VISIBLE);
            }
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getJSON(String url,String status_filter,String tahun_filter) {
        class GetJSON extends AsyncTask<Void,Void,String> {
            ProgressDialog loading;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(FragmentOrdersHistory.this.getContext(),"Mengambil Data","Mohon Tunggu...",false,false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                JSON_STRING = s;
                if(url.contains("getHtrans")){
                    showHtrans();
                }
            }

            @Override
            protected String doInBackground(Void... params) {
                mAuth = FirebaseAuth.getInstance();
                HashMap<String,String> parameters = new HashMap<>();
                parameters.put(Configuration.STATUS_FILTER_HTRANS, status_filter);
                parameters.put(Configuration.TAHUN_FILTER_HTRANS, tahun_filter);
                parameters.put(Configuration.USER_UID, mAuth.getCurrentUser().getUid());
                RequestHandler rh = new RequestHandler();

                String s = rh.sendPostRequest(url,parameters);
                return s;
            }
        }

        GetJSON gj = new GetJSON();
        gj.execute();
    }
}
