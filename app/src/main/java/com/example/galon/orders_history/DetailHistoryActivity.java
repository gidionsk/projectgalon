//package com.example.galon.orders_history;
//
//import androidx.appcompat.app.AppCompatActivity;
//
//import android.graphics.Color;
//import android.os.Bundle;
//import android.view.View;
//
//import com.example.galon.databinding.ActivityDetailHistoryBinding;
//
//public class DetailHistoryActivity extends AppCompatActivity {
//    ActivityDetailHistoryBinding binding;
//    String[] progress ={"Pembayaran","Terkonfirmasi","Pengiriman","Selesai"};
//    int current = 0;
//
//    String pembayaran,approval,metode,waktu,grandtotal,id,nama,alamat,nohp,tanggal;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        binding=ActivityDetailHistoryBinding.inflate(getLayoutInflater());
//        setContentView(binding.getRoot());
//
//        Bundle extras = getIntent().getExtras();
//        if(extras != null) {
//            pembayaran = extras.getString("pembayaran");
//            approval = extras.getString("approval");
//            metode = extras.getString("metode");
//            waktu = extras.getString("waktu");
//            tanggal = extras.getString("tanggal");
//            grandtotal = extras.getString("grandtotal");
//            id = extras.getString("id");
//            nama = extras.getString("nama_penerima");
//            alamat = extras.getString("alamat_penerima");
//            nohp = extras.getString("no_hp");
//        }
//
//        if(pembayaran.equals("SUCCESS")){
//            if(approval.equals("PENDING")){
//                current=1;
//            }
//            else if(approval.equals("SENDING")){
//                current=2;
//            }
//            else if(approval.equals("SUCCESS")){
//                current=3;
//            }
//        }
//
//        binding.detailHistoriPbStep.setLabels(progress).setBarColorIndicator(Color.GRAY).setProgressColorIndicator(Color.GREEN).setLabelColorIndicator(Color.BLACK).setCompletedPosition(0).drawView();
//        binding.detailHistoriPbStep.setCompletedPosition(current);
//
//        binding.detailHistoryTvGrandTotal.setText("Rp."+String.format("%,d", Long.parseLong(grandtotal)));
//        binding.detailHistoryTvTanggalTransaksi.setText(tanggal);
//        binding.detailHistoryTvWaktuTransaksi.setText(waktu);
//        binding.detailHistoryTvMetodePembayaran.setText(metode);
//        binding.detailHistoryTvIdTransaksi.setText(id);
//        binding.detailHistoryTvNamaPenerima.setText(nama);
//        binding.detailHistoryTvAlamatPenerima.setText(alamat);
//        binding.detailHistoryTvNoHpPenerima.setText(nohp);
//
//        if(pembayaran.equals("PENDING")){
//            binding.detailHistoryTvStatusTransaksi.setText("Menunggu Pembayaran");
//        }
//        else if(pembayaran.equals("SUCCESS")){
//            if(approval.equals("PENDING")){
//                binding.detailHistoryTvStatusTransaksi.setText("Menunggu Konfirmasi");
//            }
//            else if(approval.equals("SENDING")){
//                binding.detailHistoryTvStatusTransaksi.setText("Proses Pengiriman");
//            }
//            else if(approval.equals("DONE")){
//                binding.detailHistoryTvStatusTransaksi.setText("Transaksi Sukses");
//                binding.detailHistoryTvStatusTransaksi.setTextColor(Color.GREEN);
//            }
//            else{
//                binding.detailHistoryTvStatusTransaksi.setText("Transaksi Gagal");
//                binding.detailHistoryTvStatusTransaksi.setTextColor(Color.RED);
//            }
//        }
//        else{
//            binding.detailHistoryTvStatusTransaksi.setText("Transaksi Gagal");
//            binding.detailHistoryTvStatusTransaksi.setTextColor(Color.RED);
//        }
//    }
//
//    public void back(View view) {
//        finish();
//    }
//}