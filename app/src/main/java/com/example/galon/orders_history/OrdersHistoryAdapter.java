package com.example.galon.orders_history;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.galon.R;
import com.example.galon.classes.Htrans;

import java.util.ArrayList;

public class OrdersHistoryAdapter extends RecyclerView.Adapter<OrdersHistoryAdapter.OrdersHistoryViewHolder> {
    private Context context;
    private ArrayList<Htrans> listHtrans;

    public OrdersHistoryAdapter(Context context, ArrayList<Htrans> listHtrans) {
        this.context = context;
        this.listHtrans = listHtrans;
    }

    @NonNull
    @Override
    public OrdersHistoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_history, parent,false);
        OrdersHistoryViewHolder holder = new OrdersHistoryViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull OrdersHistoryAdapter.OrdersHistoryViewHolder holder, int position) {
        Htrans htrans = listHtrans.get(position);
        holder.tvTanggal.setText(htrans.getTanggal());
        holder.tvJumlahBarang.setText("Jumlah : " + htrans.getJumlah_barang()+" Barang");
        holder.tvGrandTotal.setText("Rp."+String.format("%,d", Long.parseLong(htrans.getGrand_total())));
        if(htrans.getStatus_pembayaran().equals("PENDING")){
            holder.tvStatus.setText("Menunggu Pembayaran");
        }
        else if(htrans.getStatus_pembayaran().equals("SUCCESS")){
            if(htrans.getStatus_approval().equals("PENDING")){
                holder.tvStatus.setText("Menunggu Konfirmasi");
            }
            else if(htrans.getStatus_approval().equals("SENDING")){
                holder.tvStatus.setText("Proses Pengiriman");
            }
            else if(htrans.getStatus_approval().equals("DONE")){
                holder.tvStatus.setText("Transaksi Sukses");
                holder.tvStatus.setTextColor(Color.GREEN);
            }
            else{
                holder.tvStatus.setText("Transaksi Gagal");
                holder.tvStatus.setTextColor(Color.RED);
            }
        }
        else{
            holder.tvStatus.setText("Transaksi Gagal");
            holder.tvStatus.setTextColor(Color.RED);
        }
    }

    @Override
    public int getItemCount() {
        return listHtrans.size();
    }

    public class OrdersHistoryViewHolder extends RecyclerView.ViewHolder {
        TextView tvTanggal,tvJumlahBarang,tvGrandTotal,tvStatus;

        public OrdersHistoryViewHolder(@NonNull View itemView) {
            super(itemView);
//            tvTanggal = itemView.findViewById(R.id.adapterHistory_tvTanggal);
//            tvJumlahBarang = itemView.findViewById(R.id.adapterHistory_tvJumlahBarang);
//            tvGrandTotal = itemView.findViewById(R.id.adapterHistory_tvGrandtotal);
//            tvStatus = itemView.findViewById(R.id.adapterHistory_tvStatus);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    Intent i = new Intent(context,DetailHistoryActivity.class);
//                    i.putExtra("pembayaran",listHtrans.get(getLayoutPosition()).status_pembayaran);
//                    i.putExtra("approval",listHtrans.get(getLayoutPosition()).status_approval);
//                    i.putExtra("metode",listHtrans.get(getLayoutPosition()).metode);
//                    i.putExtra("tanggal",listHtrans.get(getLayoutPosition()).tanggal);
//                    i.putExtra("waktu",listHtrans.get(getLayoutPosition()).waktu);
//                    i.putExtra("grandtotal",listHtrans.get(getLayoutPosition()).grand_total);
//                    i.putExtra("nama_penerima",listHtrans.get(getLayoutPosition()).nama_penerima);
//                    i.putExtra("alamat_penerima",listHtrans.get(getLayoutPosition()).alamat_penerima);
//                    i.putExtra("no_hp",listHtrans.get(getLayoutPosition()).nohp_penerima);
//                    i.putExtra("id",listHtrans.get(getLayoutPosition()).id);
//                    context.startActivity(i);
                }
            });
        }
    }
}

