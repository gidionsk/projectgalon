package com.example.galon.checkout

import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.location.LocationListener
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.example.galon.R
import com.github.florent37.runtimepermission.kotlin.askPermission
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import java.io.IOException
import java.util.*

class GantiAlamatActivity : AppCompatActivity(), OnMapReadyCallback, LocationListener, GoogleMap.OnCameraMoveListener, GoogleMap.OnCameraMoveStartedListener,GoogleMap.OnCameraIdleListener  {

    private var mMap: GoogleMap? = null
    lateinit var mapView: MapView
    private val MAP_VIEW_BUNDLE_KEY = "MapViewBundleKey"

    companion object {
        var resultcode:Int=20
    }

    private val DEFAULT_ZOOM = 15f

    lateinit var tvCurrentAddress: TextView

    private var fusedLocationProviderClient: FusedLocationProviderClient? = null
    var latitude: Double = 0.0
    var longitude: Double = 0.0

    override fun onMapReady(googleMap: GoogleMap?) {
        mapView.onResume()
        mMap = googleMap

        askPermissionLocation()

        if(ActivityCompat.checkSelfPermission(
                        this, android.Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                        this, android.Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED){
            return
        }
        mMap!!.setMyLocationEnabled(true)
        mMap!!.setOnCameraMoveListener(this)
        mMap!!.setOnCameraMoveStartedListener(this)
        mMap!!.setOnCameraIdleListener(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ganti_alamat)

        mapView = findViewById(R.id.map1)
        tvCurrentAddress = findViewById(R.id.tvAdd)

        askPermissionLocation()
        var mapViewBundle: Bundle? = null
        if(savedInstanceState!=null){
            mapViewBundle=savedInstanceState.getBundle(MAP_VIEW_BUNDLE_KEY)
        }
        mapView.onCreate(mapViewBundle)
        mapView.getMapAsync(this)
    }

    public override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        askPermissionLocation()
        var mapViewBundle = outState.getBundle(MAP_VIEW_BUNDLE_KEY)
        if(mapViewBundle==null){
            mapViewBundle = Bundle()
            outState.putBundle(MAP_VIEW_BUNDLE_KEY, mapViewBundle)
        }
        mapView.onSaveInstanceState(mapViewBundle)
    }

    private fun askPermissionLocation(){
        askPermission(
                android.Manifest.permission.ACCESS_FINE_LOCATION,
                android.Manifest.permission.ACCESS_COARSE_LOCATION
        ){
            getCurrentLocation()
        }.onDeclined { e ->
            if(e.hasDenied()){
                AlertDialog.Builder(this)
                        .setMessage("Please accept our permissions..Otherwise you will not able to use some of our Important Features.")
                        .setPositiveButton("yes"){ _, _ ->
                            e.askAgain()
                        }
                        .setNegativeButton("no"){ dialog, _ ->
                            dialog.dismiss()
                        }
                        .show()
            }
            if (e.hasForeverDenied()){
                e.goToSettings()
            }
        }
    }

    private fun getCurrentLocation() {
        fusedLocationProviderClient =
                LocationServices.getFusedLocationProviderClient(this@GantiAlamatActivity)
        try {
            @SuppressLint("MissingPermission")
            val location = fusedLocationProviderClient!!.getLastLocation()

            location.addOnCompleteListener(object : OnCompleteListener<Location> {
                override fun onComplete(loc: Task<Location>) {
                    if (loc.isSuccessful) {
                        val currentLocation = loc.result
                        if (currentLocation != null) {
                            moveCamera(
                                    LatLng(currentLocation.latitude, currentLocation.longitude),
                                    DEFAULT_ZOOM
                            )
                        }
                    } else {
                        askPermissionLocation()
                    }
                }
            })
        }
        catch (se:Exception){
            Log.e("TAG", "Security Exception")
        }
    }

    private fun moveCamera(latLng: LatLng, zoom: Float){
        if(mMap!=null){
            mMap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom))
        }
    }

    override fun onLocationChanged(location: Location) {
        val geocoder = Geocoder(this, Locale.getDefault())
        var addresses: List<Address>? = null
        try {
            addresses = geocoder.getFromLocation(location!!.latitude, location.longitude, 1)
        }
        catch (e: IOException){
            e.printStackTrace()
        }
        latitude = location!!.latitude
        longitude = location.longitude
        setAddress(addresses!![0])
    }

    private fun setAddress(addresses: Address) {
        if(addresses.getAddressLine(0)!=null){
            tvCurrentAddress!!.setText(addresses.getAddressLine(0))
        }
        if(addresses.getAddressLine(1)!=null){
            tvCurrentAddress!!.setText(
                    tvCurrentAddress.getText().toString() + addresses.getAddressLine(1)
            )
        }
    }

    override fun onCameraMove() {
    }

    override fun onCameraMoveStarted(p0: Int) {
    }

    override fun onCameraIdle() {
        var addresses: List<Address>? = null
        val geocoder = Geocoder(this, Locale.getDefault())
        try {
            addresses = geocoder.getFromLocation(mMap!!.cameraPosition.target.latitude, mMap!!.cameraPosition.target.longitude, 1)
            latitude = mMap!!.cameraPosition.target.latitude
            longitude = mMap!!.cameraPosition.target.longitude
            setAddress(addresses!![0])
        }
        catch (e: IndexOutOfBoundsException){
            e.printStackTrace()
        }
        catch (e: IOException){
            e.printStackTrace()
        }
    }

    fun pilihAlamat(view: View) {
        if(tvCurrentAddress!!.getText().toString().contains("SBY", ignoreCase = true) || tvCurrentAddress!!.getText().toString().contains("Surabaya", ignoreCase = true)){
            try {
                val move = Intent()
                move.putExtra("alamat", tvCurrentAddress!!.getText().toString().substringBefore(","))
                move.putExtra("lengkap", tvCurrentAddress!!.getText().toString())
                move.putExtra("latitude",latitude)
                move.putExtra("longitude",longitude)
                setResult(resultcode, move)
                finish()
            }
            finally {
                finish()
            }
        }
        else{
            Toast.makeText(this@GantiAlamatActivity, "Alamat pengiriman hanya berlaku di wilayah Surabaya saja!", Toast.LENGTH_SHORT).show()
        }
    }
}