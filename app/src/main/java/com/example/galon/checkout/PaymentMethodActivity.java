//package com.example.galon.checkout;
//
//import android.content.Intent;
//import android.os.AsyncTask;
//import android.os.Bundle;
//import android.view.View;
//import android.widget.Toast;
//
//import androidx.annotation.NonNull;
//import androidx.appcompat.app.AppCompatActivity;
//
//import com.example.galon.R;
//import com.example.galon.classes.Cart;
//import com.example.galon.classes.User;
//import com.example.galon.home.HomeActivity;
//import com.example.galon.mySQL.Configuration;
//import com.example.galon.mySQL.RequestHandler;
////import com.example.galon.order.FragmentOrder;
//import com.google.firebase.auth.FirebaseAuth;
//import com.google.firebase.database.DataSnapshot;
//import com.google.firebase.database.DatabaseError;
//import com.google.firebase.database.DatabaseReference;
//import com.google.firebase.database.FirebaseDatabase;
//import com.google.firebase.database.ValueEventListener;
//import com.midtrans.sdk.corekit.callback.TransactionFinishedCallback;
//import com.midtrans.sdk.corekit.core.MidtransSDK;
//import com.midtrans.sdk.corekit.core.PaymentMethod;
//import com.midtrans.sdk.corekit.core.themes.CustomColorTheme;
//import com.midtrans.sdk.corekit.models.BillingAddress;
//import com.midtrans.sdk.corekit.models.CustomerDetails;
//import com.midtrans.sdk.corekit.models.ShippingAddress;
//import com.midtrans.sdk.corekit.models.snap.TransactionResult;
//import com.midtrans.sdk.uikit.SdkUIFlowBuilder;
//
//import java.io.Serializable;
//import java.util.ArrayList;
//import java.util.HashMap;
//
//public class PaymentMethodActivity extends AppCompatActivity implements  Serializable {
//
//    double latitude,longitude;
//    String nama,alamat,lengkap,nohp,metode_pembayaran,id_voucher;
//    ArrayList<Cart> carts = new ArrayList<>();
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_payment_method);
//
//        Bundle extras = getIntent().getExtras();
//        if(extras != null) {
//            longitude = extras.getDouble("longitude");
//            latitude = extras.getDouble("latitude");
//            carts = (ArrayList<Cart>) extras.getSerializable("carts");
//            lengkap = extras.getString("lengkap");
//            nama = extras.getString("nama_penerima");
//            id_voucher = extras.getString("id_voucher");
//            alamat = extras.getString("alamat");
//            nohp = extras.getString("nohp");
//        }
//    }
//
//    public void bca(View view) {
//        metode_pembayaran ="Bank BCA";
//        proceed();
//    }
//
//    public void bca_klikpay(View view) {
//        metode_pembayaran ="BCA KlikPay";
//        proceed();
//    }
//
//    public void klik_bca(View view) {
//        metode_pembayaran ="Klik BCA";
//        proceed();
//    }
//
//    public void mandiri(View view) {
//        metode_pembayaran ="Bank Mandiri";
//        proceed();
//    }
//
//    public void mandiri_klikpay(View view) {
//        metode_pembayaran ="Mandiri Clickpay";
//        proceed();
//    }
//
//    public void epay_bri(View view) {
//        metode_pembayaran ="E-Pay BRI";
//        proceed();
//    }
//
//    public void danamon(View view) {
//        metode_pembayaran ="Danamon";
//        proceed();
//    }
//
//    public void bni(View view) {
//        metode_pembayaran ="Bank BNI";
//        proceed();
//    }
//
//    public void permata(View view) {
//        metode_pembayaran ="Permata";
//        proceed();
//    }
//
//    public void cimb_clicks(View view) {
//        metode_pembayaran ="CIMB Clicks";
//        proceed();
//    }
//
//    public void akulaku(View view) {
//        metode_pembayaran ="akulaku";
//        proceed();
//    }
//
//    public void gopay(View view) {
//        metode_pembayaran ="Gopay";
//        proceed();
//    }
//
//    public void shopeepay(View view) {
//        metode_pembayaran ="ShopeePay";
//        proceed();
//    }
//
//    public void indomaret(View view) {
//        metode_pembayaran ="Indomaret";
//        proceed();
//    }
//
//    public void alfamart(View view) {
//        metode_pembayaran ="Alfamart";
//        proceed();
//    }
//
//    public void tcash(View view) {
//        metode_pembayaran ="T-Cash";
//        proceed();
//    }
//
//    public void indosat(View view) {
//        metode_pembayaran ="Indosat";
//        proceed();
//    }
//
//    public void xl(View view) {
//        metode_pembayaran ="XL";
//        proceed();
//    }
//
//    public void proceed(){
////        Intent  i = new Intent(PaymentMethodActivity.this,CheckoutActivity.class);
////        i.putExtra("longitude",longitude);
////        i.putExtra("latitude",latitude);
////        i.putExtra("carts",carts);
////        i.putExtra("nama_penerima",nama);
////        i.putExtra("nohp",nohp);
////        i.putExtra("alamat",alamat);
////        i.putExtra("lengkap",lengkap);
////        i.putExtra("metode_pembayaran",metode_pembayaran);
////        i.putExtra("id_voucher",id_voucher);
////        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
////        startActivity(i);
////        finish();
//    }
//
//    public void back(View view) {
//        finish();
//    }
//}