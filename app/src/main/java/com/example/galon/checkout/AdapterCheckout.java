package com.example.galon.checkout;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.galon.R;
import com.example.galon.classes.Cart;

import java.util.ArrayList;

public class AdapterCheckout extends RecyclerView.Adapter<AdapterCheckout.DetailItemViewHolder> {

    private Context context;
    private ArrayList<Cart>  carts;
    private onListClearItemClick onListClearItemClick;
    private onTotalChangeClick onTotalChangeClick;

    public void setOnListClearItemClick(onListClearItemClick listClearItemClick){
        this.onListClearItemClick = listClearItemClick;
    }
    public void setOnTotalChangeClick(onTotalChangeClick totalChangeClick){
        this.onTotalChangeClick = totalChangeClick;
    }

    public AdapterCheckout(Context context, ArrayList<Cart> carts) {
        this.context = context;
        this.carts = carts;
    }

    @NonNull
    @Override
    public DetailItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_checkout, parent,false);
        AdapterCheckout.DetailItemViewHolder holder = new AdapterCheckout.DetailItemViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull DetailItemViewHolder holder, int position) {
        Cart cart = carts.get(position);
        holder.namaBarang.setText(cart.getProduct().namaBarang);
        Long total = Long.parseLong(cart.getProduct().hargaBarang.toString()) * cart.getQuantity();
        holder.hargaJumlahBarang.setText("Rp." + String.format("%,d",total));
        Glide.with(context).load(cart.product.gambarBarang).apply(new RequestOptions().override(300, 300)).into(holder.fotoBarang);
        holder.jumlahBarang.setText(cart.quantity+"");

        holder.btnKurang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(cart.getQuantity()-1>0){
                    cart.setQuantity(cart.getQuantity()-1);
                    Long total = Long.parseLong(cart.getProduct().hargaBarang.toString()) * cart.getQuantity();
                    holder.hargaJumlahBarang.setText("Rp." + String.format("%,d",total));
                    holder.jumlahBarang.setText(cart.getQuantity()+"");
                }
                else{
                    new AlertDialog.Builder(view.getRootView().getContext())
                    .setTitle("Hapus Produk")
                    .setMessage("Apakah anda yakin untuk menghapus produk ini dari keranjang ?")
                    .setPositiveButton("Ya, Hapus", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            cart.setQuantity(cart.getQuantity()-1);
                            carts.remove(position);
                            onListClearItemClick.onClick(true);
                        }
                    })

                    .setNegativeButton("Tidak", null)
                    .show();
                }
                onTotalChangeClick.onClick(true);
            }
        });

        holder.btnTambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cart.setQuantity(cart.getQuantity()+1);
                holder.jumlahBarang.setText(cart.getQuantity()+"");
                Long total = Long.parseLong(cart.getProduct().hargaBarang.toString()) * cart.getQuantity();
                holder.hargaJumlahBarang.setText("Rp." + String.format("%,d",total));
                holder.jumlahBarang.setText(cart.getQuantity()+"");
                onTotalChangeClick.onClick(true);
            }
        });
    }

    @Override
    public int getItemCount() {
        return carts.size();
    }

    public class DetailItemViewHolder extends RecyclerView.ViewHolder {
        ImageView fotoBarang;
        TextView namaBarang,hargaJumlahBarang,jumlahBarang;
        Button btnKurang,btnTambah;
        public DetailItemViewHolder(@NonNull View itemView) {
            super(itemView);
            btnKurang = itemView.findViewById(R.id.adapterCheckout_btnKurang);
            btnTambah = itemView.findViewById(R.id.adapterCheckout_btnTambah);
            jumlahBarang = itemView.findViewById(R.id.adapterCheckout_tvJumlahItem);
            namaBarang = itemView.findViewById(R.id.adapterCheckout_tvNamaBarang);
            hargaJumlahBarang = itemView.findViewById(R.id.adapterCheckout_tvHargaJumlah);
            fotoBarang = itemView.findViewById(R.id.adapterCheckout_ivFotoBarang);
        }
    }
}
