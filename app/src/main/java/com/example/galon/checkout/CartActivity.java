//package com.example.galon.checkout;
//
//import android.animation.ValueAnimator;
//import android.app.ProgressDialog;
//import android.content.Intent;
//import android.graphics.Paint;
//import android.os.AsyncTask;
//import android.os.Bundle;
//import android.view.View;
//import android.widget.EditText;
//import android.widget.ImageView;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import androidx.annotation.NonNull;
//import androidx.annotation.Nullable;
//import androidx.appcompat.app.AppCompatActivity;
//import androidx.recyclerview.widget.LinearLayoutManager;
//import androidx.recyclerview.widget.RecyclerView;
//
//import com.example.galon.R;
//import com.example.galon.classes.Voucher;
//import com.example.galon.home.HomeActivity;
//import com.example.galon.mySQL.Configuration;
//import com.example.galon.mySQL.RequestHandler;
//import com.example.galon.voucher.VoucherActivity;
//import com.google.firebase.auth.FirebaseAuth;
//import com.google.firebase.database.DataSnapshot;
//import com.google.firebase.database.DatabaseError;
//import com.google.firebase.database.DatabaseReference;
//import com.google.firebase.database.FirebaseDatabase;
//import com.google.firebase.database.ValueEventListener;
//import com.midtrans.sdk.corekit.callback.TransactionFinishedCallback;
//import com.midtrans.sdk.corekit.core.MidtransSDK;
//import com.midtrans.sdk.corekit.core.PaymentMethod;
//import com.midtrans.sdk.corekit.core.themes.CustomColorTheme;
//import com.midtrans.sdk.corekit.models.BillingAddress;
//import com.midtrans.sdk.corekit.models.CustomerDetails;
//import com.midtrans.sdk.corekit.models.ShippingAddress;
//import com.midtrans.sdk.corekit.models.snap.TransactionResult;
//import com.midtrans.sdk.uikit.SdkUIFlowBuilder;
//
//import org.json.JSONArray;
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.io.Serializable;
//import java.text.DateFormat;
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.HashMap;
//
//public class CartActivity extends AppCompatActivity implements TransactionFinishedCallback, Serializable {
//    FirebaseAuth firebaseAuth;
//    DatabaseReference mDatabase;
//
//    AdapterCheckout adapterCheckout;
//    RecyclerView rvDetailItem;
//    TextView tvJudulDetailPesanan,tvTotalPesanan,tvMetodePembayaran,tvAlamat,tvVoucher,tvPotongan;
//    EditText etNama,etNohp;
//    ImageView ivMetodePembayaran;
//
//    ArrayList<Voucher> listVoucher = new ArrayList<>();
//    String lengkap;
//    int total = 0;
//    int totalBarang =0;
//    double longitude;
//    double latitude;
//    int potongan_diskon = 0;
//    PaymentMethod metode = null;
//    String id_voucher = "";
//    String postalcode="";
//    String nama_penerima,nohp,alamat,response,status;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_cart);
//        firebaseAuth = FirebaseAuth.getInstance();
//        rvDetailItem = findViewById(R.id.checkout_rvListCart);
//
//        tvAlamat = findViewById(R.id.checkout_tvIsiAlamatPemesan);
//        tvJudulDetailPesanan = findViewById(R.id.checkout_tvJudulDetailPesanan);
//        tvTotalPesanan = findViewById(R.id.checkout_tvTotalPesanan);
//        tvMetodePembayaran = findViewById(R.id.checkout_tvMetodePembayaran);
//        tvVoucher = findViewById(R.id.checkout_tvVoucher);
//        ivMetodePembayaran = findViewById(R.id.checkout_ivMetodePembayaran);
//        tvPotongan = findViewById(R.id.checkout_tvPotongan);
//
//        etNama = findViewById(R.id.checkout_etNamaPemesan);
//        etNohp = findViewById(R.id.checkout_etNohpPemesan);
//        rvDetailItem.setHasFixedSize(true);
//        rvDetailItem.setLayoutManager(new LinearLayoutManager(this));
//        rvDetailItem.setNestedScrollingEnabled(false);
//
//        Bundle extras = getIntent().getExtras();
//        if(extras != null) {
//            Intent fromHome = getIntent();
////            carts = (ArrayList<Cart>) fromHome.getSerializableExtra("carts");
//
//            if(extras.getString("metode_pembayaran")!=null){
//                longitude = extras.getDouble("longitude");
//                latitude = extras.getDouble("latitude");
////                carts = (ArrayList<Cart>) extras.getSerializable("carts");
//                lengkap = extras.getString("lengkap");
//
//                tvAlamat.setText(extras.getString("alamat"));
//                etNohp.setText(extras.getString("nohp"));
//                etNama.setText(extras.getString("nama_penerima"));
//                tvMetodePembayaran.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
//                tvMetodePembayaran.setText(extras.getString("metode_pembayaran"));
//                paymentMethod(tvMetodePembayaran.getText().toString());
//
//                id_voucher = extras.getString("id_voucher");
//                if(!id_voucher.equals("")){
//                    getJSON(Configuration.URL_GET_VOUCHER,id_voucher);
//                    tvPotongan.setVisibility(View.VISIBLE);
//                }
//                else{
//                    tvPotongan.setVisibility(View.GONE);
//                }
//            }
//            else if(extras.getSerializable("dataString")!=null){
//                ArrayList<String> dataString = (ArrayList<String>) extras.getSerializable("dataString");
//                longitude = Double.parseDouble(dataString.get(0));
//                latitude = Double.parseDouble(dataString.get(1));
////                carts = (ArrayList<Cart>) extras.getSerializable("carts");
//                lengkap = dataString.get(2);
//
//                tvAlamat.setText(dataString.get(4));
//                etNohp.setText(dataString.get(5));
//                etNama.setText(dataString.get(3));
//
//                String metodePembayaran = dataString.get(6);
//                if(metodePembayaran!=null){
//                    tvMetodePembayaran.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
//                    tvMetodePembayaran.setText(metodePembayaran);
//                    paymentMethod(tvMetodePembayaran.getText().toString());
//                }
//                else{
//                    ivMetodePembayaran.setVisibility(View.GONE);
//                    tvMetodePembayaran.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
//                    tvMetodePembayaran.setText("Pilih Pembayaran");
//                }
//
//                id_voucher = extras.getString("id_voucher");
//                if(!id_voucher.equals("")){
//                    getJSON(Configuration.URL_GET_VOUCHER,id_voucher);
//                    tvPotongan.setVisibility(View.VISIBLE);
//                }
//                else{
//                    tvPotongan.setVisibility(View.GONE);
//                }
//            }
//            else{
//                ivMetodePembayaran.setVisibility(View.GONE);
//                tvMetodePembayaran.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
//                tvMetodePembayaran.setText("Pilih Pembayaran");
//
//                ProgressDialog loading;
//                loading = ProgressDialog.show(CartActivity.this,"Mengambil Data...","Mohon Menunggu...",false,false);
//
//                mDatabase = FirebaseDatabase.getInstance().getReference();
//                final DatabaseReference databaseReference = mDatabase.child("users");
//                databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
//                    @Override
//                    public void onDataChange(@NonNull DataSnapshot snapshot) {
//                        for (DataSnapshot ds:snapshot.getChildren()){
//                            if(ds.child("email").getValue().toString().equals(firebaseAuth.getCurrentUser().getEmail())){
//                                loading.dismiss();
//                                tvAlamat.setText(ds.child("alamat").getValue().toString());
//                                longitude = Double.parseDouble(ds.child("longitude").getValue().toString());
//                                latitude = Double.parseDouble(ds.child("latitude").getValue().toString());
//                                etNohp.setText(ds.child("nohp").getValue().toString());
//                                etNama.setText(ds.child("nama").getValue().toString());
//                            }
//                        }
//                    }
//
//                    @Override
//                    public void onCancelled(@NonNull DatabaseError error) {}
//                });
//
//            }
//        }
//
////        adapterCheckout = new AdapterCheckout(this.getApplicationContext(),carts);
//        adapterCheckout.notifyDataSetChanged();
//        rvDetailItem.setAdapter(adapterCheckout);
//
//        adapterCheckout.setOnListClearItemClick(new onListClearItemClick() {
//            @Override
//            public void onClick(Boolean change) {
//                if(change==true){
//                    adapterCheckout.notifyDataSetChanged();
//                }
//            }
//        });
//
//        adapterCheckout.setOnTotalChangeClick(new onTotalChangeClick() {
//            @Override
//            public void onClick(Boolean change) {
//                if(change==true){
//                    hitungTotal(true);
//                }
//            }
//        });
//
//        tvJudulDetailPesanan.setText("Detail Pesanan ("+getDateToday()+")");
//
////        for (int i=0;i<carts.size();i++){
////            total = total + (carts.get(i).getProduct().getHargaBarang() * carts.get(i).getQuantity());
////            totalBarang = totalBarang + carts.get(i).getQuantity();
////        }
//        tvTotalPesanan.setText("Total Pesanan : Rp. "+String.format("%,d", Long.parseLong(String.valueOf(total)))+",-");
//
//        initMidtransSkd();
//    }
//
//    public void hitungTotal(Boolean change){
//        if(change){
//            int total_awal = total;
////            if(carts.size()==0){
////                total=0;
////            }
////            else{
////                total = 0;
////                for (int i=0;i<carts.size();i++){
////                    total = total + (carts.get(i).getProduct().getHargaBarang() * carts.get(i).getQuantity());
////                    totalBarang = totalBarang + carts.get(i).getQuantity();
////                }
////            }
//            startCountAnimation(total_awal,total);
//        }
//    }
//
//    private void startCountAnimation(int total_awal,int total_akhir) {
//        ValueAnimator animator = ValueAnimator.ofInt(total_awal, total_akhir);
//        animator.setDuration(1000);
//        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
//            public void onAnimationUpdate(ValueAnimator animation) {
//                tvTotalPesanan.setText("Total Pesanan : Rp. "+String.format("%,d", Long.parseLong(String.valueOf(total)))+",-");
//            }
//        });
//        animator.start();
//    }
//
//
//    public String getDateToday(){
//        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
//        Date date = new Date();
//        String today = dateFormat.format(date);
//        return today;
//    }
//
//    private void initMidtransSkd(){
//        SdkUIFlowBuilder.init().setContext(this)
//                .setMerchantBaseUrl("https://projectgalon2021.herokuapp.com/index.php/")
//                .setClientKey("SB-Mid-client-t5mWzqEiDumR4fm5").setTransactionFinishedCallback(this)
//                .enableLog(true)
//                .setColorTheme(new CustomColorTheme("#FFE51255", "#B61548", "#FFE51255")) // set theme. it will replace theme on snap theme on MAP ( optional)
//                .buildSDK();
//    }
//
//
//    public void back(View view) {
//        this.finish();
//    }
//
//    public void gantiAlamat(View view) {
//        Intent i = new Intent(CartActivity.this, GantiAlamatActivity.class);
//        i.putExtra("nama",etNama.toString());
//        i.putExtra("nohp",etNohp.toString());
//        startActivityForResult(i,10);
//    }
//
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if(requestCode==requestCode){
//            if(resultCode==GantiAlamatActivity.Companion.getResultcode()){
//                tvAlamat.setText(data.getStringExtra("alamat"));
//                longitude = data.getDoubleExtra("longitude",0);
//                latitude = data.getDoubleExtra("latitude",0);
//                lengkap = data.getStringExtra("lengkap");
//            }
//        }
//    }
//
//    public void proceed(){
//        int index_spasi = etNama.getText().toString().indexOf(' ');
//        String firstname = etNama.getText().toString().substring(0,index_spasi);
//        String lastname = etNama.getText().toString().substring(index_spasi);
//
//        CustomerDetails customerDetails = new CustomerDetails();
//        customerDetails.setCustomerIdentifier(etNama.getText().toString());
//        customerDetails.setPhone(etNohp.getText().toString());
//        customerDetails.setFirstName(firstname);
//        customerDetails.setLastName(lastname);
//        customerDetails.setEmail(firebaseAuth.getCurrentUser().getEmail());
//
//        ShippingAddress shippingAddress = new ShippingAddress();
//        shippingAddress.setAddress(tvAlamat.getText().toString());
//        shippingAddress.setCity("Surabaya");
//
//        if(lengkap!=null){
//            int postalcode1 = lengkap.indexOf("601");
//            int postalcode2 = lengkap.indexOf("602");
//            if(postalcode1>0){
//                postalcode = lengkap.substring(postalcode1,postalcode1+5);
//            }
//            else if(postalcode2>0){
//                postalcode = lengkap.substring(postalcode2,postalcode2+5);
//            }
//            shippingAddress.setPostalCode(postalcode);
//            shippingAddress.setCountryCode("IDN");
//
//            BillingAddress billingAddress = new BillingAddress();
//            billingAddress.setAddress(tvAlamat.getText().toString());
//            billingAddress.setCity("Surabaya");
//            billingAddress.setPostalCode(postalcode);
//            billingAddress.setCountryCode("IDN");
//
//            customerDetails.setShippingAddress(shippingAddress);
//            customerDetails.setBillingAddress(billingAddress);
//
////            MidtransSDK.getInstance().setTransactionRequest(User.transactionRequest("1",total,totalBarang, carts,customerDetails,tvMetodePembayaran.getText().toString(),potongan_diskon));
//            MidtransSDK.getInstance().startPaymentUiFlow(CartActivity.this, metode);
//        }
//        else{
//            mDatabase = FirebaseDatabase.getInstance().getReference();
//            final DatabaseReference databaseReference = mDatabase.child("users");
//            databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
//                @Override
//                public void onDataChange(@NonNull DataSnapshot snapshot) {
//                    for (DataSnapshot ds:snapshot.getChildren()){
//                        if(ds.child("email").getValue().toString().equals(firebaseAuth.getCurrentUser().getEmail())){
//                            postalcode = ds.child("postal").getValue().toString();
//                            shippingAddress.setPostalCode(postalcode);
//                            shippingAddress.setCountryCode("IDN");
//
//                            BillingAddress billingAddress = new BillingAddress();
//                            billingAddress.setAddress(tvAlamat.getText().toString());
//                            billingAddress.setCity("Surabaya");
//                            billingAddress.setPostalCode(postalcode);
//                            billingAddress.setCountryCode("IDN");
//
//                            customerDetails.setShippingAddress(shippingAddress);
//                            customerDetails.setBillingAddress(billingAddress);
//
////                            MidtransSDK.getInstance().setTransactionRequest(User.transactionRequest("2",total,totalBarang, carts,customerDetails,tvMetodePembayaran.getText().toString(),potongan_diskon));
//                            MidtransSDK.getInstance().startPaymentUiFlow(CartActivity.this, metode);
//                        }
//                    }
//                }
//                @Override
//                public void onCancelled(@NonNull DatabaseError error) {}
//            });
//        }
//    }
//
//    @Override
//    public void onTransactionFinished(TransactionResult result) {
//        if(result.getResponse()!=null){
//            response = result.getResponse().getTransactionId();
//
//            switch(result.getStatus()){
//                case TransactionResult.STATUS_SUCCESS:
//                    Toast.makeText(this, "Transaction Finished ID :" +result.getResponse().getTransactionId(), Toast.LENGTH_SHORT).show();
//                    status = "SUCCESS";
//
//                    addTransaction();
//                    detailTransaction();
//
//                    break;
//                case TransactionResult.STATUS_PENDING:
//                    Toast.makeText(this, "Transaction Pending ID : "+result.getResponse().getTransactionId(), Toast.LENGTH_SHORT).show();
//                    status = "PENDING";
//
//                    addTransaction();
//                    detailTransaction();
//
//                    break;
//                case TransactionResult.STATUS_FAILED:
//                    Toast.makeText(this, "Transaction Failed ID : "+result.getResponse().getTransactionId(), Toast.LENGTH_SHORT).show();
//                    status = "FAILED";
//                    break;
//            }
//
//            Intent toHome = new Intent(CartActivity.this, HomeActivity.class);
//            startActivity(toHome);
//
//            result.getResponse().getValidationMessages();
//        }
//    }
//
//    private void addTransaction(){
//        firebaseAuth = FirebaseAuth.getInstance();
//        final String UID = firebaseAuth.getCurrentUser().getUid();
//        final String totalHarga2 = String.valueOf(total).toString().trim();
//        final String idVoucher = id_voucher;
//        final String totalBarang2 = String.valueOf(totalBarang).toString().trim();
//        final String latitude_fix = String.valueOf(latitude).toString().trim();
//        final String longitude_fix = String.valueOf(longitude).toString().trim();
//        final String nama_penerima2 = etNama.getText().toString();
//        final String nohp2 = etNohp.getText().toString();
//        final String alamat2 = tvAlamat.getText().toString();
//        final String metode_pembayaran2 = tvMetodePembayaran.getText().toString();
//        final String response2 = response;
//        final String status2 = status;
//        class AddTransaction extends AsyncTask<Void,Void,String> {
//
//            @Override
//            protected void onPreExecute() {
//                super.onPreExecute();
//            }
//
//            @Override
//            protected void onPostExecute(String s) {
//                super.onPostExecute(s);
//            }
//            String res;
//
//            @Override
//            protected String doInBackground(Void... v) {
//                HashMap<String,String> params = new HashMap<>();
//                params.put(Configuration.USER_LATITUDE, latitude_fix);
//                params.put(Configuration.USER_LONGITUDE, longitude_fix);
//                params.put(Configuration.PRODUCT_QUANTITY, totalBarang2);
//                params.put(Configuration.PRODUCT_TOTALPRICE, totalHarga2);
//                params.put(Configuration.USER_UID,UID);
//                params.put(Configuration.USER_NAME,nama_penerima2);
//                params.put(Configuration.USER_PHONE,nohp2);
//                params.put(Configuration.USER_ADDRESS,alamat2);
//                params.put(Configuration.PAYMENT_METHOD,metode_pembayaran2);
//                params.put(Configuration.TRANSACTION_ID, response2);
//                params.put(Configuration.PAYMENT_STATUS, status2);
//                params.put(Configuration.USER_LOGINNAME, firebaseAuth.getCurrentUser().getDisplayName());
//                params.put(Configuration.VOUCHER_ID,idVoucher);
//
//                RequestHandler rh = new RequestHandler();
//                res = rh.sendPostRequest(Configuration.URL_ADD_HTRANS, params);
//                return res;
//            }
//        }
//        AddTransaction ae = new AddTransaction();
//        ae.execute();
//    }
//
//    private void detailTransaction(){
//
//        class DetailTransaction extends AsyncTask<Void,Void,String> {
//
//            @Override
//            protected void onPreExecute() {
//                super.onPreExecute();
//            }
//
//            @Override
//            protected void onPostExecute(String s) {
//                super.onPostExecute(s);
//            }
//            String res;
//
//            @Override
//            protected String doInBackground(Void... v) {
////                for (int i=0;i<carts.size();i++){
////                    HashMap<String,String> params = new HashMap<>();
////                    int totalPriceItem = carts.get(i).getQuantity() * carts.get(i).getProduct().getHargaBarang();
////                    params.put(Configuration.PRODUCT_QUANTITY, String.valueOf(carts.get(i).getQuantity()));
////                    params.put(Configuration.PRODUCT_PRICE, String.valueOf(carts.get(i).getProduct().getHargaBarang()));
////                    params.put(Configuration.PRODUCT_TOTALPRICE, String.valueOf(totalPriceItem));
////                    params.put(Configuration.PRODUCT_NAME,carts.get(i).getProduct().getNamaBarang());
////
////                    RequestHandler rh = new RequestHandler();
////                    res = rh.sendPostRequest(Configuration.URL_ADD_DTRANS, params);
////                }
//                return res;
//            }
//        }
//        DetailTransaction ae = new DetailTransaction();
//        ae.execute();
//    }
//
//    public void pilihPembayaran(View view) {
////        Intent  i = new Intent(CheckoutActivity.this,PaymentMethodActivity.class);
////        i.putExtra("totalHarga",total);
////        i.putExtra("totalBarang",totalBarang);
////        i.putExtra("longitude",longitude);
////        i.putExtra("latitude",latitude);
////        i.putExtra("carts",carts);
////        i.putExtra("nama_penerima",etNama.getText().toString());
////        i.putExtra("nohp",etNohp.getText().toString());
////        i.putExtra("alamat",tvAlamat.getText().toString());
////        i.putExtra("lengkap",lengkap);
////        i.putExtra("id_voucher",id_voucher);
////        startActivity(i);
//    }
//
//    public void paymentMethod(String metode_pembayaran){
//        if(metode_pembayaran.equals("Bank BCA")){
//            ivMetodePembayaran.setImageResource(R.drawable.ic_bca);
//            metode = PaymentMethod.BANK_TRANSFER_BCA;
//        }
//        else if(metode_pembayaran.equals("BCA KlikPay")){
//            ivMetodePembayaran.setImageResource(R.drawable.ic_klikpay);
//            metode = PaymentMethod.BCA_KLIKPAY;
//        }
//        else if(metode_pembayaran.equals("Klik BCA")){
//            ivMetodePembayaran.setImageResource(R.drawable.ic_klikbca);
//            metode = PaymentMethod.KLIKBCA;
//        }
//        else if(metode_pembayaran.equals("Bank Mandiri")){
//            ivMetodePembayaran.setImageResource(R.drawable.ic_mandiri_bill_payment2);
//            metode = PaymentMethod.BANK_TRANSFER_MANDIRI;
//        }
//        else if(metode_pembayaran.equals("Mandiri Clickpay")){
//            ivMetodePembayaran.setImageResource(R.drawable.ic_mandiri2);
//            metode = PaymentMethod.MANDIRI_CLICKPAY;
//        }
//        else if(metode_pembayaran.equals("E-Pay BRI")){
//            ivMetodePembayaran.setImageResource(R.drawable.ic_epay);
//            metode = PaymentMethod.EPAY_BRI;
//        }
//        else if(metode_pembayaran.equals("Danamon")){
//            ivMetodePembayaran.setImageResource(R.drawable.ic_danamon_online);
//            metode = PaymentMethod.DANAMON_ONLINE;
//        }
//        else if(metode_pembayaran.equals("Bank BNI")){
//            ivMetodePembayaran.setImageResource(R.drawable.ic_bni);
//            metode = PaymentMethod.BANK_TRANSFER_BNI;
//        }
//        else if(metode_pembayaran.equals("Permata")){
//            ivMetodePembayaran.setImageResource(0);
//            metode = PaymentMethod.BANK_TRANSFER_PERMATA;
//        }
//        else if(metode_pembayaran.equals("akulaku")){
//            ivMetodePembayaran.setImageResource(R.drawable.ic_akulaku);
//            metode = PaymentMethod.AKULAKU;
//        }
//        else if(metode_pembayaran.equals("CIMB Clicks")){
//            ivMetodePembayaran.setImageResource(R.drawable.cimb);
//            metode = PaymentMethod.CIMB_CLICKS;
//        }
//        else if(metode_pembayaran.equals("Gopay")){
//            ivMetodePembayaran.setImageResource(R.drawable.ic_gopay);
//            metode = PaymentMethod.GO_PAY;
//        }
//        else if(metode_pembayaran.equals("ShopeePay")){
//            ivMetodePembayaran.setImageResource(R.drawable.uikit_ic_shopeepay);
//            metode = PaymentMethod.SHOPEEPAY;
//        }
//        else if(metode_pembayaran.equals("Indomaret")){
//            ivMetodePembayaran.setImageResource(R.drawable.ic_indomaret);
//            metode = PaymentMethod.INDOMARET;
//        }
//        else if(metode_pembayaran.equals("Alfamart")){
//            ivMetodePembayaran.setImageResource(R.drawable.ic_alfamart);
//            metode = PaymentMethod.ALFAMART;
//        }
//        else if(metode_pembayaran.equals("T-Cash")){
//            ivMetodePembayaran.setImageResource(R.drawable.ic_telkomsel);
//            metode = PaymentMethod.TELKOMSEL_CASH;
//        }
//        else if(metode_pembayaran.equals("XL")){
//            ivMetodePembayaran.setImageResource(R.drawable.ic_xl);
//            metode = PaymentMethod.XL_TUNAI;
//        }
//        else if(metode_pembayaran.equals("Indosat")) {
//            ivMetodePembayaran.setImageResource(R.drawable.ic_indosat);
//            metode = PaymentMethod.INDOSAT_DOMPETKU;
//        }
//    }
//
//    private String JSON_STRING;
//    private void showVoucher() {
//        JSONObject jsonObject = null;
//        try {
//
//            jsonObject = new JSONObject(JSON_STRING);
//            JSONArray result = jsonObject.getJSONArray(Configuration.TAG_JSON_ARRAY);
//
//            for(int i = 0; i<result.length(); i++){
//                JSONObject jo = result.getJSONObject(i);
//                String id = jo.getString("id");
//                String judul = jo.getString("judul");
//                String deskripsi = jo.getString("deskripsi");
//                String tanggal_mulai = jo.getString("tanggal_mulai");
//                String tanggal_akhir = jo.getString("tanggal_akhir");
//                String minim_total_pembelian = jo.getString("minim_total_pembelian");
//                String minim_jumlah_produk = jo.getString("minim_jumlah_produk");
//                String limit_use = jo.getString("limit_use");
//                String max_diskon= jo.getString("max_diskon");
//                String persen_diskon= jo.getString("persen_diskon");
//                String potongan_diskon = jo.getString("potongan_diskon");
//                String foto = jo.getString("foto");
//
//                Voucher tempVoucher = new Voucher(id,judul,deskripsi,tanggal_mulai,tanggal_akhir,minim_total_pembelian,minim_jumlah_produk,limit_use,max_diskon,persen_diskon,potongan_diskon,foto);
//                listVoucher.add(tempVoucher);
//            }
//            tvVoucher.setText(listVoucher.get(0).judul);
//            tvVoucher.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_success,0);
//            tvPotongan.setText("Rp. "+String.format("%,d", Long.parseLong(String.valueOf(total)))+",-");
//            tvPotongan.setPaintFlags(tvPotongan.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
//
//            if(Integer.parseInt(listVoucher.get(0).potongan_diskon)>0){
//                total-=Integer.parseInt(listVoucher.get(0).potongan_diskon);
//                potongan_diskon = Integer.parseInt(listVoucher.get(0).potongan_diskon);
//            }
//            else {
//                potongan_diskon = total * Integer.parseInt(listVoucher.get(0).persen_diskon) / 100;
//                if (Integer.parseInt(listVoucher.get(0).max_diskon) > 0 && potongan_diskon > Integer.parseInt(listVoucher.get(0).max_diskon)) {
//                    potongan_diskon = Integer.parseInt(listVoucher.get(0).max_diskon);
//                }
//                total -= potongan_diskon;
//            }
//            tvTotalPesanan.setText("Total Pesanan : Rp. " + String.format("%,d", Long.parseLong(String.valueOf(total))) + ",-");
//        }
//        catch (JSONException e) {
//            e.printStackTrace();
//        }
//    }
//
//    private void getJSON(String url,String id) {
//        class GetJSON extends AsyncTask<Void,Void,String> {
//            ProgressDialog loading;
//            @Override
//            protected void onPreExecute() {
//                super.onPreExecute();
//                loading = ProgressDialog.show(CartActivity.this,"Mengambil Data","Mohon Tunggu...",false,false);
//            }
//
//            @Override
//            protected void onPostExecute(String s) {
//                super.onPostExecute(s);
//                loading.dismiss();
//                JSON_STRING = s;
//                if(url.contains("getVoucher")){
//                    showVoucher();
//                }
//            }
//
//            @Override
//            protected String doInBackground(Void... params) {
//                HashMap<String,String> parameters = new HashMap<>();
//                parameters.put(Configuration.VOUCHER_ID, id);
//                RequestHandler rh = new RequestHandler();
//
//                String s = rh.sendPostRequest(url,parameters);
//                return s;
//            }
//        }
//
//        GetJSON gj = new GetJSON();
//        gj.execute();
//    }
//
//    public void voucher(View view) {
//        Intent i = new Intent(CartActivity.this, VoucherActivity.class);
////        i.putExtra("carts",carts);
//        i.putExtra("totalHarga",total);
//        if(metode!=null){
//            i.putExtra("metode",tvMetodePembayaran.getText().toString());
//        }
//        i.putExtra("totalBarang",totalBarang);
//        i.putExtra("longitude",longitude);
//        i.putExtra("latitude",latitude);
////        i.putExtra("carts",carts);
//        i.putExtra("nama_penerima",etNama.getText().toString());
//        i.putExtra("nohp",etNohp.getText().toString());
//        i.putExtra("alamat",tvAlamat.getText().toString());
//        i.putExtra("lengkap",lengkap);
//        startActivity(i);
//    }
//
//    public void checkout(View view) {
//        if(!tvMetodePembayaran.getText().toString().equals("Pilih Pembayaran")){
//            proceed();
//        }
//        else{
//            Toast.makeText(this, "Pilih Pembayaran Terlebih Dahulu", Toast.LENGTH_SHORT).show();
//        }
//    }
//}
