package com.example.galon.order;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.galon.R;
import com.example.galon.classes.Product;
import com.example.galon.home.HomeActivity;
import com.example.galon.mySQL.Configuration;
import com.example.galon.mySQL.RequestHandler;
import com.google.firebase.auth.FirebaseAuth;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class FragmentOrder extends Fragment {

    ProductAdapter adapProduct;
    ArrayList<Product> listProduct;
    ArrayList<String> listCart;

    public FragmentOrder(ProductAdapter adapProduct,ArrayList<Product> listProduct, ArrayList<String> listCart) {
        this.adapProduct = adapProduct;
        this.listProduct = listProduct;
        this.listCart = listCart;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_order, container, false);
    }

    FirebaseAuth firebaseAuth;
    RecyclerView rvProduct;
    ScrollView scrollView;
    LinearLayout linearInformasi;
    EditText etSearch;
    ArrayList<Button> listButton = new ArrayList<>();
    Button filterSemua,filterSnack,filterMinuman,filterFrozenfood,filterTabung,filterAlatpembersih,filterBuah,filterMinyak,filterBeras,filterBumbu,filterTelur;

    int filterActive = 0;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        scrollView = view.findViewById(R.id.order_scrollViewProduk);
        rvProduct = view.findViewById(R.id.order_rvProduk);
        linearInformasi = view.findViewById(R.id.order_linearInformasi);
        etSearch = view.findViewById(R.id.order_etSearch);

        getJSONCart(Configuration.URL_GET_CART);

        rvProduct.setHasFixedSize(true);
        rvProduct.setLayoutManager(new LinearLayoutManager(getContext()));
        rvProduct.setAdapter(adapProduct);

        scrollView.setVerticalScrollBarEnabled(false);
        scrollView.setHorizontalScrollBarEnabled(false);

        filterSemua = view.findViewById(R.id.order_btnFilterSemua);
        filterSnack = view.findViewById(R.id.order_btnFilterSnack);
        filterMinuman = view.findViewById(R.id.order_btnFilterMinuman);
        filterFrozenfood = view.findViewById(R.id.order_btnFilterFrozenFood);
        filterTabung = view.findViewById(R.id.order_btnFilterTabung);
        filterAlatpembersih = view.findViewById(R.id.order_btnFilterAlatPembersih);
        filterBuah = view.findViewById(R.id.order_btnFilterBuah);
        filterMinyak = view.findViewById(R.id.order_btnFilterMinyak);
        filterBeras = view.findViewById(R.id.order_btnFilterBeras);
        filterBumbu = view.findViewById(R.id.order_btnFilterBumbu);
        filterTelur = view.findViewById(R.id.order_btnFilterTelur);

        listButton.add(filterSemua);
        listButton.add(filterSnack);
        listButton.add(filterMinuman);
        listButton.add(filterFrozenfood);
        listButton.add(filterTabung);
        listButton.add(filterAlatpembersih);
        listButton.add(filterBuah);
        listButton.add(filterMinyak);
        listButton.add(filterBeras);
        listButton.add(filterBumbu);
        listButton.add(filterTelur);

        for (int i = 0; i < listButton.size(); i++) {
            int finalI = i;
            listButton.get(i).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listButton.get(filterActive).setBackgroundResource(R.color.zxing_transparent);
                    listButton.get(filterActive).setTextColor(getResources().getColor(R.color.new_black));
                    filterActive = finalI;

                    listButton.get(finalI).setBackgroundResource(R.drawable.style_filter_active);
                    listButton.get(finalI).setTextColor(getResources().getColor(R.color.new_white));

                    getJSON(Configuration.URL_GET_ALL,etSearch.getText().toString(),listButton.get(finalI).getText().toString(),true);
                }
            });
        }

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                getJSON(Configuration.URL_GET_ALL,etSearch.getText().toString(),listButton.get(filterActive).getText().toString(),true);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        getJSON(Configuration.URL_GET_ALL,"","Semua",false);
    }

    private String JSON_STRING;
    private void getJSON(String url,String search,String filter,Boolean checkFilter) {
        class GetJSON extends AsyncTask<Void,Void,String> {
            ProgressDialog loading;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                if(checkFilter==false){
                    loading = ProgressDialog.show(getContext(),"Mengambil Data","Mohon Tunggu...",false,false);
                }
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                if(checkFilter==false){
                    loading.dismiss();
                }
                JSON_STRING = s;
                if(url.contains("getProduct")){
                    showProducts();
                }
            }

            @Override
            protected String doInBackground(Void... params) {
                HashMap<String,String> parameters = new HashMap<>();
                parameters.put(Configuration.PRODUCT_TYPE, filter);
                if(!search.equals("")){
                    parameters.put(Configuration.PRODUCT_SEARCH, search);
                }

                RequestHandler rh = new RequestHandler();
                String s = rh.sendPostRequest(url,parameters);
                return s;
            }
        }

        GetJSON gj = new GetJSON();
        gj.execute();
    }

    private void showProducts() {
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(JSON_STRING);
            JSONArray result = jsonObject.getJSONArray(Configuration.TAG_JSON_ARRAY);
            listProduct.clear();
            for(int i = 0; i<result.length(); i++){
                JSONObject jo = result.getJSONObject(i);
                String name = jo.getString(Configuration.TAG_NAMA);
                Integer price = jo.getInt("price");
                Integer quantity = jo.getInt("quantity");
                String image = jo.getString("img_url");
                Product tempProduct = new Product(name,price,quantity,image);
                listProduct.add(tempProduct);
                adapProduct.notifyDataSetChanged();
            }
            rvProduct.setAdapter(adapProduct);
            checkProduct();
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void checkProduct(){
        if(listProduct.size()>0){
            rvProduct.setVisibility(View.VISIBLE);
            linearInformasi.setVisibility(View.GONE);
        }
        else{
            rvProduct.setVisibility(View.GONE);
            linearInformasi.setVisibility(View.VISIBLE);
        }
    }

    private String JSON_STRING2;
    private void getJSONCart(String url) {
        class GetJSON extends AsyncTask<Void,Void,String> {
            ProgressDialog loading;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(getContext(),"Mengambil Data","Mohon Tunggu...",false,false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                JSON_STRING2 = s;
                if(url.contains("getCart")){
                    showCart();
                }
            }

            @Override
            protected String doInBackground(Void... params) {
                String s = "";
                firebaseAuth = FirebaseAuth.getInstance();
                HashMap<String,String> parameters = new HashMap<>();
                parameters.put(Configuration.USER_UID, firebaseAuth.getCurrentUser().getUid());
                RequestHandler rh = new RequestHandler();

                s = rh.sendPostRequest(url,parameters);

                return s;
            }
        }

        GetJSON gj = new GetJSON();
        gj.execute();
    }

    private void showCart() {
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(JSON_STRING2);
            JSONArray result = jsonObject.getJSONArray(Configuration.TAG_JSON_ARRAY);

            listCart.clear();
            for(int i = 0; i<result.length(); i++){
                JSONObject jo = result.getJSONObject(i);
                String barang = jo.getString("barang");
                listCart.add(barang);
                adapProduct.notifyDataSetChanged();
            }
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }
}