package com.example.galon.order;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.galon.R;
import com.example.galon.classes.Cart;
import com.example.galon.classes.Htrans;
import com.example.galon.classes.Product;
import com.example.galon.home.HomeActivity;
import com.example.galon.mySQL.Configuration;
import com.example.galon.mySQL.RequestHandler;
import com.example.galon.orders_history.FragmentOrdersHistory;
import com.example.galon.orders_history.OrdersHistoryAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ProductViewHolder> {
    private Context context;
    private ArrayList<Product> listProduct;
    private ArrayList<String>  listCart;
    private FirebaseAuth mAuth;

    public ProductAdapter(Context context, ArrayList<Product> listProduct, ArrayList<String> listCart) {
        this.context = context;
        this.listProduct = listProduct;
        this.listCart = listCart;
    }

    @NonNull
    @Override
    public ProductAdapter.ProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_product, parent,false);
        ProductViewHolder holder = new ProductViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ProductAdapter.ProductViewHolder holder, int position) {
        if(position%2==0){
            Product product = listProduct.get(position);
            holder.tvNamaProdukKiri.setText(product.namaBarang+"");
            holder.tvHargaProdukKiri.setText("Rp."+String.format("%,d", Long.parseLong(product.hargaBarang.toString())));
            holder.tvStokKiri.setText("Sisa Stok : "+product.jumlahBarang);
            Glide.with(context).load(product.getGambarBarang()).apply(new RequestOptions().override(150, 150)).into(holder.ivProdukKiri);

            for (int i = 0; i < listCart.size(); i++) {
                if(listCart.get(i).equals(listProduct.get(position).getNamaBarang())){
                    holder.btnKiri.setText("Batal");
                    holder.btnKiri.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_fail,0,0,0);
                }
            }

            if(position+1<listProduct.size()){
                Product product2 = listProduct.get(position + 1);
                holder.tvNamaProdukKanan.setText(product2.namaBarang+"");
                holder.tvHargaProdukKanan.setText("Rp."+String.format("%,d", Long.parseLong(product2.hargaBarang.toString())));
                holder.tvStokKanan.setText("Sisa Stok : "+product2.jumlahBarang);
                Glide.with(context).load(product2.getGambarBarang()).apply(new RequestOptions().override(150, 150)).into(holder.ivProdukKanan);

                for (int i = 0; i < listCart.size(); i++) {
                    if(listCart.get(i).equals(listProduct.get(position+1).getNamaBarang())){
                        holder.btnKanan.setText("Batal");
                        holder.btnKanan.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_fail,0,0,0);
                    }
                }
            }
            else{
                holder.produkKanan.setVisibility(View.INVISIBLE);
            }
        }
        else{
            ViewGroup.LayoutParams params = holder.layout.getLayoutParams();
            params.height = 0;
            params.width = 0;
            holder.layout.setLayoutParams(params);
        }

        holder.btnKiri.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(holder.btnKiri.getText().toString().equals("Beli")){
                    addCart(listProduct.get(position).namaBarang);
                    Toast.makeText(context, "Produk dimasukkan ke dalam keranjang", Toast.LENGTH_SHORT).show();
                    holder.btnKiri.setText("Batal");
                    holder.btnKiri.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_fail,0,0,0);
                }
                else{
                    deleteCart(listProduct.get(position).namaBarang);
                    Toast.makeText(context, "Produk dihapus dari keranjang", Toast.LENGTH_SHORT).show();
                    holder.btnKiri.setText("Beli");
                    holder.btnKiri.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0);
                }

//                if(carts.size()==0){
//                    carts.add(new Cart(listProduct.get(position),1));
//                    Toast.makeText(context, "Produk ditambahkan ke dalam keranjang !", Toast.LENGTH_SHORT).show();
//                }
//                else{
//                    Boolean checkBarang = false;
//                    for (int i = 0; i < carts.size(); i++) {
//                        if(carts.get(i).getProduct().namaBarang.equals(listProduct.get(position).getNamaBarang())){
//                            checkBarang=true;
//                        }
//                    }
//                    if(checkBarang==false){
//                        carts.add(new Cart(listProduct.get(position),1));
//                        Toast.makeText(context, "Produk ditambahkan ke dalam keranjang !", Toast.LENGTH_SHORT).show();
//                    }
//                    else{
//                        Toast.makeText(context, "Produk sudah ada di keranjang anda !", Toast.LENGTH_SHORT).show();
//                    }
//                }
//                int index = -1;
//                if(carts.size()!=0){
//                    for (int i=0;i<carts.size();i++){
//                        if(carts.get(i).getProduct().getNamaBarang().equals(listProduct.get(position).getNamaBarang())){
//                            index = i;
//                            break;
//                        }
//                    }
//                    if(index==-1){
//                        if(listProduct.get(position).jumlahBarang==0){
//                            Toast.makeText(context, "Stok produk ini habis!", Toast.LENGTH_SHORT).show();
//                        }
//                    }
//                    else{
//                        if(carts.get(index).getQuantity()+1>listProduct.get(position).jumlahBarang){
//                            Toast.makeText(context, "Jumlah melebihi sisa produk!", Toast.LENGTH_SHORT).show();
//                        }
//                        else{
//                            carts.get(index).setQuantity(carts.get(index).getQuantity()+1);
//                            holder.tvJumlahBarang.setText(carts.get(index).getQuantity()+"");
//                        }
//                    }
//                }
//                else{
//                    if(listProduct.get(position).jumlahBarang==0){
//                        Toast.makeText(context, "Stok produk ini habis!", Toast.LENGTH_SHORT).show();
//                    }
//                    else{
//
//                        holder.tvJumlahBarang.setText("1");
//                    }
//                }
//                if(mListener!=null){
//                    mListener.onClick(true);
//                }
//                holder.linearBeli.setVisibility(View.GONE);
//                holder.linearQuantity.setVisibility(View.VISIBLE);
            }
        });

        holder.btnKanan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(holder.btnKanan.getText().toString().equals("Beli")){
                    addCart(listProduct.get(position+1).namaBarang);
                    Toast.makeText(context, "Produk dimasukkan ke dalam keranjang", Toast.LENGTH_SHORT).show();
                    holder.btnKanan.setText("Batal");
                    holder.btnKanan.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_fail,0,0,0);
                }
                else{
                    deleteCart(listProduct.get(position+1).namaBarang);
                    Toast.makeText(context, "Produk dihapus dari keranjang", Toast.LENGTH_SHORT).show();
                    holder.btnKanan.setText("Beli");
                    holder.btnKanan.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0);
                }
//                if(carts.size()==0){
//                    carts.add(new Cart(listProduct.get(position+1),1));
//                    Toast.makeText(context, "Produk ditambahkan ke dalam keranjang !", Toast.LENGTH_SHORT).show();
//                }
//                else{
//                    Boolean checkBarang = false;
//                    for (int i = 0; i < carts.size(); i++) {
//                        if(carts.get(i).getProduct().namaBarang.equals(listProduct.get(position+1).getNamaBarang())){
//                            checkBarang=true;
//                        }
//                    }
//                    if(checkBarang==false){
//                        carts.add(new Cart(listProduct.get(position+1),1));
//                        Toast.makeText(context, "Produk ditambahkan ke dalam keranjang !", Toast.LENGTH_SHORT).show();
//                    }
//                    else{
//                        Toast.makeText(context, "Produk sudah ada di keranjang anda !", Toast.LENGTH_SHORT).show();
//                    }
//                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return listProduct.size();
    }

    public class ProductViewHolder extends RecyclerView.ViewHolder {
        ImageView ivProdukKiri,ivProdukKanan;
        TextView tvNamaProdukKiri,tvHargaProdukKiri,tvStokKiri,tvNamaProdukKanan,tvHargaProdukKanan,tvStokKanan;
        LinearLayout layout,produkKanan;
        Button btnKiri,btnKanan;

        public ProductViewHolder(@NonNull View itemView) {
            super(itemView);
            layout = itemView.findViewById(R.id.adapterProduk_layout);
            produkKanan = itemView.findViewById(R.id.adapterProduk_kanan);
            ivProdukKiri = itemView.findViewById(R.id.adapterProduk_ivProdukKiri);
            ivProdukKanan = itemView.findViewById(R.id.adapterProduk_ivProdukKanan);
            tvNamaProdukKiri = itemView.findViewById(R.id.adapterProduk_tvNamaProdukKiri);
            tvNamaProdukKanan = itemView.findViewById(R.id.adapterProduk_tvNamaProdukKanan);
            tvHargaProdukKiri = itemView.findViewById(R.id.adapterProduk_tvHargaProdukKiri);
            tvHargaProdukKanan = itemView.findViewById(R.id.adapterProduk_tvHargaProdukKanan);
            tvStokKiri = itemView.findViewById(R.id.adapterProduk_tvStokProdukKiri);
            tvStokKanan = itemView.findViewById(R.id.adapterProduk_tvStokProdukKanan);
            btnKiri = itemView.findViewById(R.id.adapterProduk_btnProdukKiri);
            btnKanan = itemView.findViewById(R.id.adapterProduk_btnProdukKanan);
        }
    }

    private void addCart(String barang){
        final String UID = mAuth.getInstance().getCurrentUser().getUid();
        final String nama_barang = barang;

        class addProcess extends AsyncTask<Void,Void,String> {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
            }
            String res;

            @Override
            protected String doInBackground(Void... v) {
                HashMap<String,String> params = new HashMap<>();
                params.put(Configuration.USER_UID, UID);
                params.put(Configuration.PRODUCT_NAME, nama_barang);

                RequestHandler rh = new RequestHandler();
                res = rh.sendPostRequest(Configuration.URL_ADD_CART, params);
                return res;
            }
        }
        addProcess ap = new addProcess();
        ap.execute();
    }

    private void deleteCart(String barang){
        final String UID = mAuth.getInstance().getCurrentUser().getUid();
        final String nama_barang = barang;

        class addProcess extends AsyncTask<Void,Void,String> {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
            }
            String res;

            @Override
            protected String doInBackground(Void... v) {
                HashMap<String,String> params = new HashMap<>();
                params.put(Configuration.USER_UID, UID);
                params.put(Configuration.PRODUCT_NAME, nama_barang);

                RequestHandler rh = new RequestHandler();
                res = rh.sendPostRequest(Configuration.URL_DELETE_CART, params);
                return res;
            }
        }
        addProcess ap = new addProcess();
        ap.execute();
    }
}

