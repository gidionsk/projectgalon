package com.example.galon.login;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.galon.R;
import com.example.galon.classes.User;
import com.example.galon.home.HomeActivity;
import com.example.galon.mySQL.Configuration;
import com.example.galon.mySQL.RequestHandler;
import com.example.galon.register.RegisterActivity;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.HashMap;

public class LoginActivity extends AppCompatActivity {
    GoogleSignInClient mGoogleSignInClient;
    FirebaseAuth mAuth;
    int RC_SIGN_IN = 0;
    DatabaseReference databaseReference;
    AppCompatButton btnSignin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        btnSignin = findViewById(R.id.login_btnLogin);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestIdToken(getString(R.string.default_web_client_id)).requestEmail().build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        mAuth = FirebaseAuth.getInstance();

        //Rounded Button
        GradientDrawable drawable_button = new GradientDrawable();
        drawable_button.setShape(GradientDrawable.RECTANGLE);
        drawable_button.setCornerRadius(100);
        drawable_button.setColor(Color.WHITE);
        btnSignin.setBackground(drawable_button);
    }

    public void signin(View view) {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            firebaseAuthWithGoogle(account.getIdToken(),account);
        }
        catch (ApiException e) {
            Toast.makeText(this, "Login Gagal : Error" + e.getStatusCode(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);

        if(account!=null){
            String email = account.getEmail();
            checkUser(email);
        }
    }

    private void firebaseAuthWithGoogle(String idToken, GoogleSignInAccount account) {
        AuthCredential credential = GoogleAuthProvider.getCredential(idToken, null);
        mAuth.signInWithCredential(credential).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    String email = account.getEmail();
                    checkUser(email);
                }
                else {
                    Toast.makeText(LoginActivity.this, "Login Gagal : Error"+ task.getException(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    ProgressDialog loading;
    public void checkUser(String email){
        loading = ProgressDialog.show(LoginActivity.this,"Verifikasi Data...","Mohon Menunggu...",false,false);

        databaseReference = FirebaseDatabase.getInstance().getReference().child("users");
        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                Boolean check = false;
                for (DataSnapshot ds:snapshot.getChildren()){
                    if(ds.child("email").getValue().toString().equals(email)){
                        check=true;

                        User update = new User();
                        update.setEmail(ds.child("email").getValue().toString());
                        update.setAlamat(ds.child("alamat").getValue().toString());
                        update.setNama(ds.child("nama").getValue().toString());
                        update.setNohp(ds.child("nohp").getValue().toString());
                        update.setPostal("123");

                        update.setToken( FirebaseInstanceId.getInstance().getToken());
                        databaseReference.child(ds.getKey()).setValue(update);

                        addUser();
                    }
                }
                loading.dismiss();
                if(check==false){
                    Intent intent = new Intent(LoginActivity.this,RegisterActivity.class);
                    startActivity(intent);
                }
                else{
                    Intent intent = new Intent(LoginActivity.this,HomeActivity.class);
                    startActivity(intent);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {}
        });
    }

    private void addUser(){
        final String UID = mAuth.getCurrentUser().getUid();
        final String Token = FirebaseInstanceId.getInstance().getToken();

        class addProcess extends AsyncTask<Void,Void,String> {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
            }
            String res;

            @Override
            protected String doInBackground(Void... v) {
                HashMap<String,String> params = new HashMap<>();
                params.put(Configuration.USER_UID, UID);
                params.put(Configuration.USER_TOKEN, Token);

                RequestHandler rh = new RequestHandler();
                res = rh.sendPostRequest(Configuration.URL_ADD_USER, params);
                return res;
            }
        }
        addProcess ap = new addProcess();
        ap.execute();
    }
}