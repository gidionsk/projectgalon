package com.example.galon.register;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.galon.R;
import com.example.galon.checkout.GantiAlamatActivity;
import com.example.galon.classes.User;
import com.example.galon.home.HomeActivity;
import com.example.galon.mySQL.Configuration;
import com.example.galon.mySQL.RequestHandler;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.HashMap;

public class RegisterActivity extends AppCompatActivity {

    FirebaseAuth mAuth ;
    DatabaseReference mDatabase;
    String nama, email,UID;
    EditText etNohp;
    Button simpan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        etNohp = findViewById(R.id.register_etNoHP);
        simpan = findViewById(R.id.register_btnVerifikasi);

        GoogleSignInAccount acc = GoogleSignIn.getLastSignedInAccount(this);
        mAuth = FirebaseAuth.getInstance();
        nama = acc.getDisplayName();
        email = acc.getEmail();
        UID = mAuth.getCurrentUser().getUid();
    }

    ProgressDialog loading;
    public void simpan(View view) {
        loading = ProgressDialog.show(RegisterActivity.this,"Verifikasi Data...","Mohon Menunggu...",false,false);
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Users");
        mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                loading.dismiss();
                if(etNohp.getText().toString().equals("")){
                    Toast.makeText(RegisterActivity.this, "Isi semua data terlebih dahulu!", Toast.LENGTH_LONG).show();
                }
                else if(etNohp.length()<11||etNohp.length()>13){
                    Toast.makeText(getApplicationContext(), "Nomor telepon tidak valid!", Toast.LENGTH_SHORT).show();
                }
                else {
                    Boolean check = false;
                    for (DataSnapshot ds:snapshot.getChildren()){
                        if(ds.child("nohp").getValue().toString().equals(etNohp.getText().toString())){
                            check=true;
                        }
                    }

                    if(check==true){
                        Toast.makeText(RegisterActivity.this, "Nomor handphone sudah digunakan!", Toast.LENGTH_SHORT).show();
                    }
                    else{
                        addUser();

                        mDatabase = FirebaseDatabase.getInstance().getReference();
                        User user = new User(email, nama,etNohp.getText().toString(),"Belum diatur","","","", FirebaseInstanceId.getInstance().getToken());
                        mDatabase.child("users").child(UID).setValue(user);

                        Intent intent = new Intent(RegisterActivity.this, HomeActivity.class);
                        startActivity(intent);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {}
        });
    }

    private void addUser(){
        final String UID = mAuth.getCurrentUser().getUid();
        final String Token = FirebaseInstanceId.getInstance().getToken();

        class addProcess extends AsyncTask<Void,Void,String> {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
            }
            String res;

            @Override
            protected String doInBackground(Void... v) {
                HashMap<String,String> params = new HashMap<>();
                params.put(Configuration.USER_UID, UID);
                params.put(Configuration.USER_TOKEN, Token);

                RequestHandler rh = new RequestHandler();
                res = rh.sendPostRequest(Configuration.URL_ADD_USER, params);
                return res;
            }
        }
        addProcess ap = new addProcess();
        ap.execute();
    }

}