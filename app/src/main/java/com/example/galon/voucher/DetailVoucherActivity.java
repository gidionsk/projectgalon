package com.example.galon.voucher;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;

import com.example.galon.R;
import com.example.galon.checkout.CartActivity;
import com.example.galon.classes.Cart;
import com.example.galon.databinding.ActivityDetailVoucherBinding;

import java.io.Serializable;
import java.util.ArrayList;

public class DetailVoucherActivity extends AppCompatActivity implements Serializable {
    ActivityDetailVoucherBinding binding;

    String id,judul,deskripsi,tanggal_mulai,tanggal_akhir,minim_total_pembelian,minim_jumlah_produk,limit_use,max_diskon,persen_diskon,potongan_diskon,foto;
    String [] arrayDeskripsi;
    Boolean use=false;
    String id_voucher = "";

    ArrayList<String> dataString = new ArrayList<>();
    ArrayList<Cart> carts = new ArrayList<>();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityDetailVoucherBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            id = extras.getString("id");
            judul = extras.getString("judul");
            deskripsi = extras.getString("deskripsi");
            tanggal_mulai = extras.getString("tanggal_mulai");
            tanggal_akhir = extras.getString("tanggal_akhir");
            minim_total_pembelian = extras.getString("minim_total_pembelian");
            minim_jumlah_produk = extras.getString("minim_jumlah_produk");
            limit_use = extras.getString("limit_use");
            max_diskon = extras.getString("max_diskon");
            persen_diskon = extras.getString("persen_diskon");
            potongan_diskon = extras.getString("potongan_diskon");
            foto = extras.getString("foto");
            use = extras.getBoolean("use");
            if(use==true){
                binding.detailVoucherBtnGunakanVoucher.setVisibility(View.VISIBLE);
                dataString = (ArrayList<String>) extras.getSerializable("dataString");
                carts = (ArrayList<Cart>) extras.getSerializable("carts");
                id_voucher = extras.getString("id_voucher");
                binding.detailVoucherBtnGunakanVoucher.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent i = new Intent(DetailVoucherActivity.this, CartActivity.class);
                        i.putExtra("id_voucher",id_voucher);
                        i.putExtra("carts",carts);
                        i.putExtra("dataString",dataString);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(i);
                        finish();
                    }
                });
            }
        }

        binding.detailVoucherTvJudulVoucher.setText(judul);
        binding.detailVoucherTvTanggal.setText("Berlaku : "+tanggal_mulai+" - "+tanggal_akhir);
        arrayDeskripsi = deskripsi.split(",");
        for (int i = 0; i < arrayDeskripsi.length; i++) {
            LinearLayout linearDynamic = new LinearLayout(this);
            linearDynamic.setOrientation(LinearLayout.HORIZONTAL);
            ((LinearLayout) binding.detailVoucherLinearDeskripsi).addView(linearDynamic);

            TextView tvDynamic = new TextView(this);
            tvDynamic.setTextColor(Color.rgb(75,79,62));
            tvDynamic.setTextSize(16);
            tvDynamic.setTypeface(ResourcesCompat.getFont(this, R.font.quicksand_medium));
            tvDynamic.setText("∙");
            tvDynamic.setPadding(50,0,0,0);
            ((LinearLayout) linearDynamic).addView(tvDynamic);

            TextView tvDynamic2 = new TextView(this);
            tvDynamic2.setTextColor(Color.rgb(75,79,62));
            tvDynamic2.setTextSize(16);
            tvDynamic2.setTypeface(ResourcesCompat.getFont(this, R.font.quicksand_medium));
            tvDynamic2.setText(arrayDeskripsi[i]);
            tvDynamic2.setPadding(50,0,50,0);
            ((LinearLayout) linearDynamic).addView(tvDynamic2);
        }

        binding.detailVoucherIvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}
