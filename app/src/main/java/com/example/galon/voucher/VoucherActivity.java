package com.example.galon.voucher;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.galon.classes.Cart;
import com.example.galon.classes.Htrans;
import com.example.galon.classes.Voucher;
import com.example.galon.databinding.ActivityVoucherBinding;
import com.example.galon.mySQL.Configuration;
import com.example.galon.mySQL.RequestHandler;
import com.example.galon.orders_history.FragmentOrdersHistory;
import com.example.galon.orders_history.OrdersHistoryAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

public class VoucherActivity extends AppCompatActivity implements Serializable {

    ActivityVoucherBinding binding;
    VoucherAdapter voucherAdapter;
    public ArrayList<Voucher> listVoucher = new ArrayList<>();
    public ArrayList<Cart> carts = new ArrayList<>();

    double latitude,longitude;
    String nama,alamat,lengkap,nohp,metode_pembayaran;

    ArrayList<String> dataString = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityVoucherBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        binding.voucherRvVoucher.setHasFixedSize(true);
        binding.voucherRvVoucher.setLayoutManager(new LinearLayoutManager(this));

        getJSON(Configuration.URL_GET_VOUCHER);

        Intent fromHome = getIntent();
        if(fromHome!=null){
            carts = (ArrayList<Cart>) fromHome.getSerializableExtra("carts");
        }

        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            if(extras.getString("metode")!=null) {
                metode_pembayaran= extras.getString("metode");
            }
            longitude = extras.getDouble("longitude");
            latitude = extras.getDouble("latitude");
            carts = (ArrayList<Cart>) extras.getSerializable("carts");
            lengkap = extras.getString("lengkap");
            nama = extras.getString("nama_penerima");
            alamat = extras.getString("alamat");
            nohp = extras.getString("nohp");

            dataString.add(longitude+"");
            dataString.add(latitude+"");
            dataString.add(lengkap);
            dataString.add(nama);
            dataString.add(alamat);
            dataString.add(nohp);
            dataString.add(metode_pembayaran);
        }

        binding.voucherIvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private String JSON_STRING;
    private void showVoucher() {
        JSONObject jsonObject = null;
        try {

            jsonObject = new JSONObject(JSON_STRING);
            JSONArray result = jsonObject.getJSONArray(Configuration.TAG_JSON_ARRAY);

            for(int i = 0; i<result.length(); i++){
                JSONObject jo = result.getJSONObject(i);
                String id = jo.getString("id");
                String judul = jo.getString("judul");
                String deskripsi = jo.getString("deskripsi");
                String tanggal_mulai = jo.getString("tanggal_mulai");
                String tanggal_akhir = jo.getString("tanggal_akhir");
                String minim_total_pembelian = jo.getString("minim_total_pembelian");
                String minim_jumlah_produk = jo.getString("minim_jumlah_produk");
                String limit_use = jo.getString("limit_use");
                String max_diskon= jo.getString("max_diskon");
                String persen_diskon= jo.getString("persen_diskon");
                String potongan_diskon = jo.getString("potongan_diskon");
                String foto = jo.getString("foto");

                Voucher tempVoucher = new Voucher(id,judul,deskripsi,tanggal_mulai,tanggal_akhir,minim_total_pembelian,minim_jumlah_produk,limit_use,max_diskon,persen_diskon,potongan_diskon,foto);
                listVoucher.add(tempVoucher);
                voucherAdapter = new VoucherAdapter(this,listVoucher,"Lihat Voucher",carts,dataString);
                voucherAdapter.notifyDataSetChanged();
            }
            binding.voucherRvVoucher.setAdapter(voucherAdapter);

            if(result.length()>0){
                binding.voucherRvVoucher.setVisibility(View.VISIBLE);
                binding.voucherLinearInformasi.setVisibility(View.GONE);
            }
            else{
                binding.voucherRvVoucher.setVisibility(View.GONE);
                binding.voucherLinearInformasi.setVisibility(View.VISIBLE);
            }
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getJSON(String url) {
        class GetJSON extends AsyncTask<Void,Void,String> {
            ProgressDialog loading;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(VoucherActivity.this,"Mengambil Data","Mohon Tunggu...",false,false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                JSON_STRING = s;
                if(url.contains("getVoucher")){
                    showVoucher();
                }
            }

            @Override
            protected String doInBackground(Void... params) {
                RequestHandler rh = new RequestHandler();
                String s = rh.sendGetRequest(url);
                return s;
            }
        }

        GetJSON gj = new GetJSON();
        gj.execute();
    }
}
