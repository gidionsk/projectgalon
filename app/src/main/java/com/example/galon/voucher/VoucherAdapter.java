package com.example.galon.voucher;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.galon.R;
import com.example.galon.checkout.CartActivity;
import com.example.galon.classes.Cart;
import com.example.galon.classes.Voucher;

import java.io.Serializable;
import java.util.ArrayList;

public class VoucherAdapter extends RecyclerView.Adapter<VoucherAdapter.VoucherViewHolder> implements Serializable {

    private Context context;
    private ArrayList<Voucher> listVoucher;
    private ArrayList<Cart> carts;
    private ArrayList<String> dataString;
    private String mode;
    private Boolean use = false;
    private String id;

    public VoucherAdapter(Context context, ArrayList<Voucher> listVoucher,String mode, ArrayList<Cart> carts,ArrayList<String> dataString) {
        this.context = context;
        this.listVoucher = listVoucher;
        this.mode= mode;
        this.carts = carts;
        this.dataString = dataString;
    }

    @NonNull
    @Override
    public VoucherViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_voucher, parent,false);
        VoucherAdapter.VoucherViewHolder holder = new VoucherAdapter.VoucherViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull VoucherViewHolder holder, int position) {
        Voucher voucher = listVoucher.get(position);
        holder.tvJudul.setText(voucher.getJudul());
        holder.tvTanggal.setText("Expired : "+voucher.getTanggal_akhir());
        holder.tvAction.setText(mode);

        if(carts!=null){
            int total_harga = 0;
            int total_barang = 0;
            for (int i = 0; i < carts.size() ; i++) {
                total_harga+=carts.get(i).product.hargaBarang*carts.get(i).quantity;
                total_barang+=carts.get(i).quantity;
            }
            if(total_harga>= Integer.parseInt(voucher.minim_total_pembelian ) && total_barang>= Integer.parseInt(voucher.minim_jumlah_produk)){
                holder.tvAction.setText("Gunakan Voucher");
                id=listVoucher.get(position).id;
                use=true;
                holder.tvAction.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent i = new Intent(context, CartActivity.class);
                        i.putExtra("id_voucher",listVoucher.get(position).id);
                        i.putExtra("carts",carts);
                        i.putExtra("dataString",dataString);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        context.startActivity(i);
                    }
                });
            }
        }
    }

    @Override
    public int getItemCount() {
        return listVoucher.size();
    }

    public class VoucherViewHolder extends RecyclerView.ViewHolder {
        TextView tvJudul,tvTanggal,tvAction;
        public VoucherViewHolder(@NonNull View itemView) {
            super(itemView);
            tvJudul = itemView.findViewById(R.id.adapterVoucher_tvJudul);
            tvTanggal = itemView.findViewById(R.id.adapterVoucher_tvTanggal);
            tvAction = itemView.findViewById(R.id.adapterVoucher_tvAction);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(context, DetailVoucherActivity.class);
                    i.putExtra("id",listVoucher.get(getLayoutPosition()).id);
                    i.putExtra("judul",listVoucher.get(getLayoutPosition()).judul);
                    i.putExtra("deskripsi",listVoucher.get(getLayoutPosition()).deskripsi);
                    i.putExtra("tanggal_mulai",listVoucher.get(getLayoutPosition()).tanggal_mulai);
                    i.putExtra("tanggal_akhir",listVoucher.get(getLayoutPosition()).tanggal_akhir);
                    i.putExtra("minim_total_pembelian",listVoucher.get(getLayoutPosition()).minim_total_pembelian);
                    i.putExtra("minim_jumlah_produk",listVoucher.get(getLayoutPosition()).minim_jumlah_produk);
                    i.putExtra("limit_use",listVoucher.get(getLayoutPosition()).limit_use);
                    i.putExtra("max_diskon",listVoucher.get(getLayoutPosition()).max_diskon);
                    i.putExtra("persen_diskon",listVoucher.get(getLayoutPosition()).persen_diskon);
                    i.putExtra("potongan_diskon",listVoucher.get(getLayoutPosition()).potongan_diskon);
                    i.putExtra("foto",listVoucher.get(getLayoutPosition()).foto);
                    i.putExtra("id_voucher",id);
                    i.putExtra("carts",carts);
                    i.putExtra("dataString",dataString);
                    i.putExtra("use",use);
                    context.startActivity(i);
                }
            });
        }
    }
}
