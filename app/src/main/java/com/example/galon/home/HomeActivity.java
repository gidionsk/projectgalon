package com.example.galon.home;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.galon.R;
import com.example.galon.classes.Htrans;
import com.example.galon.classes.Product;
import com.example.galon.databinding.ActivityHomeBinding;
import com.example.galon.databinding.ActivityVoucherBinding;
import com.example.galon.login.LoginActivity;
import com.example.galon.mySQL.Configuration;
import com.example.galon.mySQL.RequestHandler;
import com.example.galon.notifications.FragmentNotifications;
import com.example.galon.order.FragmentOrder;
import com.example.galon.order.ProductAdapter;
import com.example.galon.orders_history.FragmentOrdersHistory;
import com.example.galon.orders_history.OrdersHistoryAdapter;
import com.example.galon.profile.FragmentProfile;
import com.example.galon.voucher.VoucherActivity;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class HomeActivity extends AppCompatActivity {

    ActivityHomeBinding binding;

    GoogleSignInClient mGoogleSignInClient;
    FirebaseAuth firebaseAuth;
    ActionBarDrawerToggle toggle;
    DatabaseReference mDatabase;
    ProductAdapter adapProduct;
    ArrayList<Product> listProduct = new ArrayList<>();
    ArrayList<String> listCart = new ArrayList<>();

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityHomeBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        getJSON(Configuration.URL_GET_ALL);
        getJSON(Configuration.URL_GET_CART);

        adapProduct = new ProductAdapter(this.getApplicationContext(),listProduct,listCart);
        adapProduct.notifyDataSetChanged();

        firebaseAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        final DatabaseReference databaseReference = mDatabase.child("users");
        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for (DataSnapshot ds:snapshot.getChildren()){
                    if(ds.child("email").getValue().toString().equals(firebaseAuth.getCurrentUser().getEmail())){
                        String[] namaDepan = ds.child("nama").getValue().toString().split(" ");
                        binding.homeTvWelcome.setText("Selamat Datang, "+namaDepan[0]+" !");
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {}
        });

        toggle = new ActionBarDrawerToggle(this,binding.homeDrawer,R.string.open,R.string.close);
        binding.homeDrawer.addDrawerListener(toggle);
        toggle.syncState();

        changeFragment(new FragmentOrder(adapProduct,listProduct,listCart));
        binding.homeBottomNavBar.setSelectedItemId(R.id.itemOrder);
        binding.homeBottomNavBar.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                if(menuItem.getItemId()== R.id.itemOrder){
                    changeFragment(new FragmentOrder(adapProduct,listProduct,listCart));
                }
                else if(menuItem.getItemId()==R.id.itemHistory){
                    changeFragment(new FragmentOrdersHistory());
                }
                else if(menuItem.getItemId()==R.id.itemCart){
//                    changeFragment(new FragmentCart());
                }
                else if(menuItem.getItemId()==R.id.itemProfile){
                    changeFragment(new FragmentProfile());
                }
                else{
                    changeFragment(new FragmentNotifications());
                }
                return true;
            }
        });

        binding.homeNavView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                if(item.getItemId()==R.id.itemVoucher){
                    Intent i = new Intent(HomeActivity.this, VoucherActivity.class);
                    startActivity(i);
                }
                else{
                    Signout();
                }
                return true;
            }
        });
    }

    public void Signout(){
        new AlertDialog.Builder(this)
        .setTitle("Logout")
        .setMessage("Apakah anda yakin untuk keluar dari akun anda ?")

        .setPositiveButton("Yakin", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                logout();
            }
        })

        .setNegativeButton("Tidak", null)
        .show();
    }

    public void logout(){
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        FirebaseAuth.getInstance().signOut();

        mGoogleSignInClient.signOut().addOnCompleteListener(this, new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.option_menu,menu);
        String[] namaDepan = firebaseAuth.getCurrentUser().getDisplayName().split(" ");
        menu.getItem(0).setTitle("Welcome, " + namaDepan[0]+"!" );
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(toggle.onOptionsItemSelected(item)){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void changeFragment(Fragment f) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        Bundle bundle = new Bundle();
        f.setArguments(bundle);
        ft.replace(R.id.home_frameLayout, f);
        ft.commit();
    }

    public void open_hamburger(View view) {
        binding.homeDrawer.openDrawer(GravityCompat.END);
    }

    private String JSON_STRING;
    private void getJSON(String url) {
        class GetJSON extends AsyncTask<Void,Void,String> {
            ProgressDialog loading;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(HomeActivity.this,"Mengambil Data","Mohon Tunggu...",false,false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                JSON_STRING = s;
                if(url.contains("getProduct")){
                    showProducts();
                }
                else{
                    showCart();
                }
            }

            @Override
            protected String doInBackground(Void... params) {
                String s = "";
                if(url.contains("getProduct")){
                    RequestHandler rh = new RequestHandler();
                    s = rh.sendGetRequest(url);
                }
                else{
                    firebaseAuth = FirebaseAuth.getInstance();
                    HashMap<String,String> parameters = new HashMap<>();
                    parameters.put(Configuration.USER_UID, firebaseAuth.getCurrentUser().getUid());
                    RequestHandler rh = new RequestHandler();

                    s = rh.sendPostRequest(url,parameters);
                }
                return s;
            }
        }

        GetJSON gj = new GetJSON();
        gj.execute();
    }

    private void showProducts() {
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(JSON_STRING);
            JSONArray result = jsonObject.getJSONArray(Configuration.TAG_JSON_ARRAY);

            for(int i = 0; i<result.length(); i++){
                JSONObject jo = result.getJSONObject(i);
                String id = jo.getString(Configuration.TAG_ID);
                String name = jo.getString(Configuration.TAG_NAMA);
                Integer price = jo.getInt("price");
                Integer quantity = jo.getInt("quantity");
                String image = jo.getString("img_url");
                Product tempProduct = new Product(name,price,quantity,image);
                listProduct.add(tempProduct);
                adapProduct.notifyDataSetChanged();
            }
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void showCart() {
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(JSON_STRING);
            JSONArray result = jsonObject.getJSONArray(Configuration.TAG_JSON_ARRAY);

            listCart.clear();
            for(int i = 0; i<result.length(); i++){
                JSONObject jo = result.getJSONObject(i);
                String barang = jo.getString("barang");
                listCart.add(barang);
                adapProduct.notifyDataSetChanged();
            }
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }
}