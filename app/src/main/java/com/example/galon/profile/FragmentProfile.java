package com.example.galon.profile;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.galon.R;
import com.example.galon.checkout.GantiAlamatActivity;
import com.example.galon.classes.User;
import com.example.galon.databinding.FragmentProfileBinding;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class FragmentProfile extends Fragment {

    public FragmentProfile() {}

    FragmentProfileBinding binding;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = binding.inflate(inflater, container, false);
        View view = binding.getRoot();
        return view;
    }

    FirebaseAuth firebaseAuth;
    DatabaseReference mDatabase;
    Boolean ganti = false;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        firebaseAuth = FirebaseAuth.getInstance();

        mDatabase = FirebaseDatabase.getInstance().getReference();
        final DatabaseReference databaseReference = mDatabase.child("users");
        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {

                for (DataSnapshot ds:snapshot.getChildren()){
                    if(ds.child("email").getValue().toString().equals(firebaseAuth.getCurrentUser().getEmail())){
                        binding.profileEtNoHP.setText(ds.child("nohp").getValue().toString());
                        binding.profileEtAlamat.setText(ds.child("alamat").getValue().toString());
                        binding.profileEtNama.setText(ds.child("nama").getValue().toString());
                        binding.profileEtEmail.setText(firebaseAuth.getCurrentUser().getEmail());
                        Glide.with(getContext()).load(R.drawable.alfamart).apply(new RequestOptions().override(200, 200)).into(binding.profileIvFoto);

                        binding.profileLinearLoading.setVisibility(View.GONE);
                        binding.profileLinearDataProfil.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {}
        });

        binding.profileEtNama.setEnabled(false);
        binding.profileEtNoHP.setEnabled(false);
        binding.profileEtAlamat.setEnabled(false);

        binding.profileBtnGanti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                update();
            }
        });
    }

    public void update() {
        if(ganti==false){
            ganti=true;
            binding.profileEtNoHP.setEnabled(true);
            binding.profileEtNama.setEnabled(true);
            binding.profileEtAlamat.setEnabled(true);
            binding.profileBtnGanti.setText("Simpan");
            binding.profileEtNoHP.requestFocus();
        }
        else{
            ganti=false;

            if(binding.profileEtNoHP.getText().toString().isEmpty() || binding.profileEtAlamat.getText().toString().isEmpty()){
                Toast.makeText(getContext(), "Lengkapi data terlebih dahulu!", Toast.LENGTH_SHORT).show();
            }
            else{
                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

                final DatabaseReference databaseReference = mDatabase.child("users");
                databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        for (DataSnapshot ds:snapshot.getChildren()){
                            if(ds.child("email").getValue().toString().equals(user.getEmail())){
                                User update = new User();
                                update.setEmail(user.getEmail());
                                update.setAlamat(binding.profileEtAlamat.getText().toString());
                                update.setNama(binding.profileEtNama.getText().toString());
                                update.setNohp(binding.profileEtNoHP.getText().toString());
//                                update.setLatitude(String.valueOf(latitude));
//                                update.setLongitude(String.valueOf(longitude));
                                databaseReference.child(ds.getKey()).setValue(update).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        binding.profileEtNoHP.setEnabled(false);
                                        binding.profileEtNama.setEnabled(false);
                                        binding.profileEtAlamat.setEnabled(false);
                                        binding.profileBtnGanti.setText("Ganti");

                                        Toast.makeText(getContext(), "Profil anda berhasil diganti!", Toast.LENGTH_SHORT).show();
                                    }
                                });
                            }
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {}
                });
            }
        }
    }

//    public void gantiAlamat(View view) {
//        if(ganti!=false){
//            Intent i = new Intent(ProfileActivity.this, GantiAlamatActivity.class);
//            i.putExtra("nama",etName.toString());
//            i.putExtra("nohp",etNoHp.toString());
//            startActivityForResult(i,10);
//        }
//    }
//
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if(requestCode==requestCode){
//            if(resultCode==GantiAlamatActivity.Companion.getResultcode()){
//                tvAddress.setText(data.getStringExtra("alamat"));
//                longitude = data.getDoubleExtra("longitude",0);
//                latitude = data.getDoubleExtra("latitude",0);
//            }
//        }
//    }
}