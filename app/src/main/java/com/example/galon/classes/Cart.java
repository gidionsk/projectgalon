package com.example.galon.classes;

import androidx.annotation.NonNull;

import java.io.Serializable;

public class Cart implements Serializable {
    public Product product;
    public int quantity;

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Cart(Product product, int quantity) {

        this.product = product;
        this.quantity = quantity;
    }


    @Override
    public String toString() {
        return this.quantity + " pcs "+ this.product.namaBarang;
    }


}
