package com.example.galon.classes;

import java.io.Serializable;

public class Product implements Serializable {
    public String namaBarang;
    public Integer hargaBarang;
    public Integer jumlahBarang;
    public String gambarBarang;

    public Product(String namaBarang, Integer hargaBarang, Integer jumlahBarang, String gambarBarang) {
        this.namaBarang = namaBarang;
        this.hargaBarang = hargaBarang;
        this.jumlahBarang = jumlahBarang;
        this.gambarBarang = gambarBarang;
    }

    public String getGambarBarang() {
        return gambarBarang;
    }

    public void setGambarBarang(String gambarBarang) {
        this.gambarBarang = gambarBarang;
    }

    public String getNamaBarang() {
        return namaBarang;
    }

    public void setNamaBarang(String namaBarang) {
        this.namaBarang = namaBarang;
    }

    public Integer getHargaBarang() {
        return hargaBarang;
    }

    public void setHargaBarang(Integer hargaBarang) {
        this.hargaBarang = hargaBarang;
    }

    public Integer getJumlahBarang() {
        return jumlahBarang;
    }

    public void setJumlahBarang(Integer jumlahBarang) {
        this.jumlahBarang = jumlahBarang;
    }

    @Override
    public String toString() {
        return this.getJumlahBarang() + " galon "+ this.getNamaBarang();
    }
}
