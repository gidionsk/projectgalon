package com.example.galon.classes;

import java.io.Serializable;

public class Voucher implements Serializable {
    public String id;
    public String judul;
    public String deskripsi;
    public String tanggal_mulai;
    public String tanggal_akhir;
    public String minim_total_pembelian;
    public String minim_jumlah_produk;
    public String limit_use;
    public String max_diskon;
    public String persen_diskon;
    public String potongan_diskon;
    public String foto;

    public Voucher(String id, String judul, String deskripsi,String tanggal_mulai, String tanggal_akhir,String minim_total_pembelian, String minim_jumlah_produk, String limit_use, String max_diskon, String persen_diskon, String potongan_diskon, String foto){
        this.id = id;
        this.judul=judul;
        this.deskripsi=deskripsi;
        this.tanggal_mulai=tanggal_mulai;
        this.tanggal_akhir=tanggal_akhir;
        this.minim_total_pembelian=minim_total_pembelian;
        this.minim_jumlah_produk=minim_jumlah_produk;
        this.limit_use=limit_use;
        this.max_diskon=max_diskon;
        this.persen_diskon=persen_diskon;
        this.potongan_diskon=potongan_diskon;
        this.foto=foto;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getTanggal_mulai() {
        return tanggal_mulai;
    }

    public void setTanggal_mulai(String tanggal_mulai) {
        this.tanggal_mulai = tanggal_mulai;
    }

    public String getTanggal_akhir() {
        return tanggal_akhir;
    }

    public void setTanggal_akhir(String tanggal_akhir) {
        this.tanggal_akhir = tanggal_akhir;
    }

    public String getMinim_total_pembelian() {
        return minim_total_pembelian;
    }

    public void setMinim_total_pembelian(String minim_total_pembelian) {
        this.minim_total_pembelian = minim_total_pembelian;
    }

    public String getMinim_jumlah_produk() {
        return minim_jumlah_produk;
    }

    public void setMinim_jumlah_produk(String minim_jumlah_produk) {
        this.minim_jumlah_produk = minim_jumlah_produk;
    }

    public String getLimit_use() {
        return limit_use;
    }

    public void setLimit_use(String limit_use) {
        this.limit_use = limit_use;
    }

    public String getMax_diskon() {
        return max_diskon;
    }

    public void setMax_diskon(String max_diskon) {
        this.max_diskon = max_diskon;
    }

    public String getPersen_diskon() {
        return persen_diskon;
    }

    public void setPersen_diskon(String persen_diskon) {
        this.persen_diskon = persen_diskon;
    }

    public String getPotongan_diskon() {
        return potongan_diskon;
    }

    public void setPotongan_diskon(String potongan_diskon) {
        this.potongan_diskon = potongan_diskon;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }
}
