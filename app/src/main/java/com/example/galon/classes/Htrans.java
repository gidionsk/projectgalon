package com.example.galon.classes;

import androidx.annotation.NonNull;

import java.io.Serializable;
import java.util.ArrayList;

public class Htrans implements Serializable {
    public String id;
    public String tanggal;
    public String waktu;
    public String jumlah_barang;
    public String grand_total;
    public String status_approval;
    public String status_pembayaran;
    public String metode;
    public String nama_penerima;
    public String nohp_penerima;
    public String alamat_penerima;

    public Htrans(String id,String tanggal,String waktu, String jumlah_barang, String grand_total, String status_approval,String status_pembayaran,String metode, String nama_penerima, String nohp_penerima, String alamat_penerima) {
        this.id = id;
        this.tanggal = tanggal;
        this.waktu = waktu;
        this.jumlah_barang = jumlah_barang;
        this.grand_total = grand_total;
        this.status_approval = status_approval;
        this.status_pembayaran = status_pembayaran;
        this.metode = metode;
        this.nama_penerima = nama_penerima;
        this.nohp_penerima = nohp_penerima;
        this.alamat_penerima = alamat_penerima;
    }

    public String getWaktu() {
        return waktu;
    }

    public void setWaktu(String waktu) {
        this.waktu = waktu;
    }

    public String getJumlah_barang() {
        return jumlah_barang;
    }

    public void setJumlah_barang(String jumlah_barang) {
        this.jumlah_barang = jumlah_barang;
    }

    public String getGrand_total() {
        return grand_total;
    }

    public void setGrand_total(String grand_total) {
        this.grand_total = grand_total;
    }

    public String getStatus_approval() {
        return status_approval;
    }

    public void setStatus_approval(String status_approval) {
        this.status_approval = status_approval;
    }

    public String getStatus_pembayaran() {
        return status_pembayaran;
    }

    public void setStatus_pembayaran(String status_pembayaran) {
        this.status_pembayaran = status_pembayaran;
    }

    public String getMetode() {
        return metode;
    }

    public void setMetode(String metode) {
        this.metode = metode;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNama_penerima() {
        return nama_penerima;
    }

    public void setNama_penerima(String nama_penerima) {
        this.nama_penerima = nama_penerima;
    }

    public String getNohp_penerima() {
        return nohp_penerima;
    }

    public void setNohp_penerima(String nohp_penerima) {
        this.nohp_penerima = nohp_penerima;
    }

    public String getAlamat_penerima() {
        return alamat_penerima;
    }

    public void setAlamat_penerima(String alamat_penerima) {
        this.alamat_penerima = alamat_penerima;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }
}
