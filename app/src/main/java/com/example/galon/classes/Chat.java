package com.example.galon.classes;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Chat {
    public User sender, recipient;
    public String message;
    public String time;

    public Chat(User sender, User recipient, String message) {
        this.sender= sender;
        this.recipient= recipient;
        this.message= message;
        this.time= this.getCurrentTime();
    }

    private String getCurrentTime() {
        Calendar calendar= Calendar.getInstance();
        SimpleDateFormat mdFormat= new SimpleDateFormat("HH::mm");
        String time= mdFormat.format(calendar.getTime());

        return time;
    }
}
