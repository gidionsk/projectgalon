package com.example.galon.classes;

import com.google.firebase.database.IgnoreExtraProperties;
import com.midtrans.sdk.corekit.core.MidtransSDK;
import com.midtrans.sdk.corekit.core.TransactionRequest;
import com.midtrans.sdk.corekit.models.BillInfoModel;
import com.midtrans.sdk.corekit.models.CustomerDetails;
import com.midtrans.sdk.corekit.models.ItemDetails;

import java.util.ArrayList;

@IgnoreExtraProperties
public class User {

    public String email;
    public String nama;
    public String nohp;
    public String alamat;
    public String latitude;
    public String longitude;
    public String postal;
    public String token;
    public User () {}

    public User(String email, String nama, String nohp, String alamat,String latitude,String longitude,String postal,String token) {
        this.email = email;
        this.nama = nama;
        this.nohp = nohp;
        this.alamat = alamat;
        this.latitude = latitude;
        this.longitude = longitude;
        this.postal = postal;
        this.token = token;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getEmail() {return email;}
    public void setEmail(String email) {this.email = email;}

    public String getNama() {return nama;}
    public void setNama(String nama) {this.nama = nama;}

    public String getNohp() {return nohp;}
    public void setNohp(String nohp) {this.nohp = nohp;}

    public String getAlamat() {return alamat;}
    public void setAlamat(String alamat) {this.alamat = alamat;}

    public String getPostal() {return postal;}

    public void setPostal(String postal) {this.postal = postal;}

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public static CustomerDetails customerDetails(){
        CustomerDetails cd = new CustomerDetails();
        cd.setFirstName("Felix");
        cd.setPhone("081234567819");
        cd.setEmail("felixlonald99@gmail.com");
        return cd;
    }

    public static TransactionRequest transactionRequest(String id,int price,int qty,ArrayList<Cart> carts,CustomerDetails customerDetails,String metodePayment,int diskon){
        TransactionRequest request = new TransactionRequest(System.currentTimeMillis()+"",price);
        request.setCustomerDetails(customerDetails);

        ArrayList<ItemDetails> itemDetails = new ArrayList<>();
        for (int i=0;i<carts.size();i++){

            ItemDetails details = new ItemDetails(i+"",carts.get(i).getProduct().getHargaBarang(),carts.get(i).getQuantity(),carts.get(i).getProduct().getNamaBarang());
            itemDetails.add(details);
        }
        if(diskon!=0){
            ItemDetails details = new ItemDetails(carts.size()+"",(-1* diskon),1,"Voucher");
            itemDetails.add(details);
        }
        request.setItemDetails(itemDetails);

        BillInfoModel billInfoModel = new BillInfoModel("1", "BILL_INFO_VALUE");

        request.setBillInfoModel(billInfoModel);

        MidtransSDK.getInstance().setTransactionRequest(request);

        return request;
    }
}