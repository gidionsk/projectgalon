package com.example.galon.mySQL;

public class Configuration {

    public static final String URL_GET_ALL = "https://yamahastsjonline.com/galon/getProduct.php";
    public static final String URL_ADD_HTRANS = "https://yamahastsjonline.com/galon/insertHtrans.php";
    public static final String URL_ADD_DTRANS = "https://yamahastsjonline.com/galon/insertDtrans.php";
    public static final String URL_GET_HTRANS = "https://yamahastsjonline.com/galon/getHtrans.php";
    public static final String URL_GET_VOUCHER = "https://yamahastsjonline.com/galon/getVoucher.php";
    public static final String URL_ADD_USER = "https://yamahastsjonline.com/galon/addUser.php";
    public static final String URL_ADD_CART = "https://yamahastsjonline.com/galon/addCart.php";
    public static final String URL_GET_CART = "https://yamahastsjonline.com/galon/getCart.php";
    public static final String URL_DELETE_CART = "https://yamahastsjonline.com/galon/deleteCart.php";

//    public static final String URL_UPDATE_UID = "https://yamahastsjonline.com/galon/updateUID.php";
//    public static final String URL_GET_ALL = "http://192.168.100.31/galon/getProduct.php";
//    public static final String URL_ADD_HTRANS = "http://192.168.100.31/galon/insertHtrans.php";
//    public static final String URL_ADD_DTRANS = "http://192.168.100.31/galon/insertDtrans.php";

//    public static final String URL_GET_ALL = "http://192.168.43.84/galon/getProduct.php";
//    public static final String URL_ADD_HTRANS = "http://192.168.43.84/galon/insertHtrans.php";
//    public static final String URL_ADD_DTRANS = "http://192.168.43.84/galon/insertDtrans.php";

    //PRODUCT
    public static final String PRODUCT_QUANTITY = "quantity";
    public static final String PRODUCT_PRICE = "price";
    public static final String PRODUCT_TOTALPRICE = "total_price";
    public static final String PRODUCT_NAME = "product_name";
    public static final String PRODUCT_TYPE = "product_type";
    public static final String PRODUCT_SEARCH = "product_search";

    //USER
    public static final String USER_UID = "id";
    public static final String USER_LONGITUDE = "longitude";
    public static final String USER_LATITUDE = "latitude";
    public static final String USER_NAME = "nama_penerima";
    public static final String USER_PHONE = "nohp";
    public static final String USER_ADDRESS = "alamat";
    public static final String USER_LOGINNAME = "nama_login";
    public static final String USER_TOKEN = "token_user";

    //PAYMENT
    public static final String PAYMENT_METHOD = "payment_method";
    public static final String PAYMENT_STATUS = "status";
    public static final String TRANSACTION_ID = "transaction_uid";

    //VOUCHER
    public static final String VOUCHER_ID = "voucher_id";


    //Send Status Filter HTRANS
    public static final String STATUS_FILTER_HTRANS = "status_filter";
    public static final String TAHUN_FILTER_HTRANS = "tahun_filter";

    //JSON Tags
    public static final String TAG_JSON_ARRAY="result";
    public static final String TAG_ID = "id";
    public static final String TAG_NAMA = "name";
    public static final String TAG_EMAIL = "email";
    public static final String TAG_ALAMAT = "alamat";
    public static final String TAG_NOMOR = "nomor";
}
